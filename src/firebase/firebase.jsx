import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/firestore';

const prodConfig = {
  apiKey: "AIzaSyBv8dn0SrFOfiqiwpEHozQJO7gTMOYK1vk",
  authDomain: "gestionweb-b32b4.firebaseapp.com",
  databaseURL: "https://gestionweb-b32b4.firebaseio.com",
  projectId: "gestionweb-b32b4",
  storageBucket: "gestionweb-b32b4.appspot.com",
  messagingSenderId: "860032952910"
};

const devConfig = {
  apiKey: "AIzaSyBv8dn0SrFOfiqiwpEHozQJO7gTMOYK1vk",
  authDomain: "gestionweb-b32b4.firebaseapp.com",
  databaseURL: "https://gestionweb-b32b4.firebaseio.com",
  projectId: "gestionweb-b32b4",
  storageBucket: "gestionweb-b32b4.appspot.com",
  messagingSenderId: "860032952910"
};

const config = process.env.NODE_ENV === 'production'
  ? prodConfig
  : devConfig;

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const firestore = firebase.firestore();
const settings = {
    /* your settings... */ timestampsInSnapshots: true
};
firestore.settings(settings);

const auth = firebase.auth();
export {
  firebase,
  firestore,
  auth,
};
