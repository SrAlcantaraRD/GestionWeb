import * as React from 'react'
import PropTypes from 'prop-types'
import { TextField, Button } from '@material-ui/core'

class TransmitterForm extends React.Component {
  constructor(props) {
    super(props)
    this.handleNext = this.handleNext.bind(this)
  }

  handleNext = () => {
    this.props.fncNextStep()
  }

  render() {
    const { name, nif, phone, contactPerson, classes, address, city } = this.props

    return (
      <div>
        <TextField
          id="name"
          label="Nombre"
          className={classes.textField}
          disabled
          value={name}
          margin="normal" />
        <TextField
          id="nif"
          label="N.I.F"
          className={classes.textField}
          disabled
          value={nif}
          margin="normal" />
        <TextField
          id="phone"
          label="Telefóno"
          className={classes.textField}
          disabled
          value={phone}
          error={!phone}
          margin="normal" />
        <TextField
          id="contactPerson"
          label="Persona de Contacto"
          className={classes.textField}
          disabled
          value={contactPerson}
          error={!contactPerson}
          margin="normal" />
        <TextField
          id="city"
          label="Ciudad"
          className={classes.textField}
          disabled
          value={city}
          error={!city}
          margin="normal" />
        <TextField
          id="address"
          label="Dirección"
          className={classes.textField}
          disabled
          value={address}
          error={!address}
          margin="normal" />
        <div>
          <Button
            variant="contained"
            color="primary"
            onClick={this.handleNext} >
            Siguiente
          </Button>
        </div>
      </div>
    )
  }
}

TransmitterForm.propTypes = {
  name: PropTypes.string.isRequired,
  nif: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  contactPerson: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  city: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  fncNextStep: PropTypes.func.isRequired,
}

export default TransmitterForm
