import { createSelector } from 'reselect';

const selectUserProfilePagepPageDomain = () => state => state.itemPages.userProfile
const getStateUserProfilePagepPageFinal = createSelector(
  selectUserProfilePagepPageDomain(),
  (userProfile) => userProfile
);

export {
  selectUserProfilePagepPageDomain,
  getStateUserProfilePagepPageFinal,
};