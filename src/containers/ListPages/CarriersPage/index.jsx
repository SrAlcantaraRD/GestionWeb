import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withStyles, Typography, IconButton, Menu, Paper, MenuItem, ListItemIcon, ListItemText } from '@material-ui/core'
import { Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core'
import Actions from './actions'
import { getStateCarriersListPageFinal } from './selectors'
import { ListItemCarrier } from 'Components'
import { history } from 'Helpers'
import * as Icons from '@material-ui/icons'
import * as routes from 'Routes'

const styles = theme => ({
  root: {
    width: '95%',
    marginTop: theme.spacing.unit * 2,
    margin: 'auto',
  },
  title: {
    textAlign: 'center',
  },
  content: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
    flex: '1 0 auto',
    padding: theme.spacing.unit,
  },
})

class CarriersListPage extends React.Component {
  constructor(props) {
    super(props)

    this.getColumns = this.getColumns.bind(this)
    this.goToNewCarrier = this.goToNewCarrier.bind(this)
    this.onChangeLockState = this.onChangeLockState.bind(this)
    this.state = {
      value: 0,
      anchorEl: null,
    }
  }
  onChangeLockState = (key, carrier) => {
    this.props.fcnChangeCarrieLock(key, carrier)
  }

  goToNewCarrier = () => {
    history.push(routes.NEW_CARRIER)
  }

  handleClose = () => {
    this.setState({ anchorEl: null })
  }

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget })
  }

  getColumns = () => {
    return (
      <TableHead>
        <TableRow>
          <TableCell>Nombre</TableCell>
          <TableCell numeric>Telefono</TableCell>
          <TableCell>NIF</TableCell>
          <TableCell numeric>Activo</TableCell>
          <TableCell></TableCell>
        </TableRow>
      </TableHead>
    )
  }

  render() {
    const { carriers} = this.props.page
    const { classes } = this.props
    const { anchorEl } = this.state

    const ITEM_HEIGHT = 48
    return (
      <div className={classes.root}>
        <Typography variant="display1" gutterBottom className={classes.title}>
          Transportistas
          <IconButton
            aria-label="More"
            aria-owns={anchorEl ? 'long-menu' : null}
            aria-haspopup="true"
            onClick={this.handleClick}>
            <Icons.MoreVert />
          </IconButton>
        </Typography>
        <Menu
          id="long-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
          PaperProps={{
            style: {
              maxHeight: ITEM_HEIGHT * 4.5,
              width: 200,
            },
          }}>
          <MenuItem className={classes.menuItem} onClick={this.goToNewCarrier}>
            <ListItemIcon >
              <Icons.PersonAdd color='primary' />
            </ListItemIcon>
            <ListItemText classes={{ primary: classes.primary }} inset primary="Añadir" />
          </MenuItem>
        </Menu>
        <Paper className={classes.content}>
          <Table >
            {this.getColumns()}
            <TableBody>
              {carriers.map((carrier, key) => {
                return (
                  <ListItemCarrier carrier={carrier} key={`key-${key}`} fcnUpdateCarrier={this.onChangeLockState} />
                )
              })}
            </TableBody>
          </Table>
        </Paper>
      </div>
    )
  }
}

CarriersListPage.propTypes = {
  classes: PropTypes.object.isRequired,
}

/* STATE SECTION */
function mapStateToProps(state) {
  const page = getStateCarriersListPageFinal(state)
  return { page }
}

/* DISPATCH SECTION */
var mapDispatchToProps = (dispatch) => {
  return {
    fcnChangeCarrieLock: (key, carrier) => {
      dispatch(Actions.updateCarrier(key, carrier))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(CarriersListPage))
