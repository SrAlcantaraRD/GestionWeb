import Constants from './constants'
import {auth} from 'Firebase'

const Actions = {
  onChaneFormInput,
  isRequestPending,
  onLogin,
  onLogout,
}

function isRequestPending(isPending) {
  return dispatch => {
    dispatch({ type: Constants.SING_IN_REQUEST_PENDING, isPending });
  }
}

function onChaneFormInput(property, value) {
  const item = {
    name: property,
    value: value
  }
  return dispatch => {
    dispatch({ type: Constants.SING_IN_CHANGE_INPUT, item });
  }
}

function onLogin(username, password) {
  return dispatch => {
    dispatch(request(true))

    auth
      .doSignInWithEmailAndPassword(username, password)
      .then(authUser => {
        dispatch(success())
      })
      .catch(error => {
        dispatch(failure(error))
      })
  }

  function request(isSubmitted) {
    return {type: Constants.SING_IN_FORM_SUBMITTED, isSubmitted}
  }
  function success(user) {
    return {type: Constants.SING_IN_REQUEST_SUCCESS }
  }
  function failure(error) {
    return {type: Constants.SING_IN_REQUEST_FAILURE, error}
  }
}

function onLogout() {
  return {type: Constants.APP_USER_LOGOUT} 
}

export default Actions