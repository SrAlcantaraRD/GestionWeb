import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import withMobileDialog from '@material-ui/core/withMobileDialog'
import Slide from '@material-ui/core/Slide';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class AlertConfirm extends React.Component {
  fcnOnCancel = (event) => {
    this.props.fcnOnCancel()
    event.preventDefault()
  }

  fcnOnAccept = (event) => {
    this.props.fcnOnAccept()
    event.preventDefault()
  }

  render() {
    const {
      strTitle,
      strDescription,
      strAgree,
      strNonAgree,
      boolOpen
    } = this.props

    return (
      <div>
        <Dialog
          open={boolOpen}
          aria-labelledby="responsive-dialog-title"
          TransitionComponent={Transition}
          aria-describedby="alert-dialog-slide-description">
          <DialogTitle id="responsive-dialog-title">{strTitle}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {strDescription}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.fcnOnCancel} color="primary">
              {strNonAgree}
            </Button>
            <Button onClick={this.fcnOnAccept} color="primary" autoFocus>
              {strAgree}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

AlertConfirm.propTypes = {
  strTitle: PropTypes.string.isRequired,
  strDescription: PropTypes.string,
  strNonAgree: PropTypes.string.isRequired,
  strAgree: PropTypes.string.isRequired,
  boolOpen: PropTypes.bool.isRequired,
  fcnOnAccept: PropTypes.func.isRequired,
  fcnOnCancel: PropTypes.func.isRequired,
}

export default withMobileDialog()(AlertConfirm)