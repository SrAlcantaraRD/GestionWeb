import { createSelector } from 'reselect';

const selectServicesListPageDomain = () => state => {
  return {
    ...state.listPages.services,
    userKey: state.app.key,
    isAdmin: state.app.isAdmin,
    carriers: state.listPages.carriers.carriers,
  }
}

const getStateServicesListPageFinal = createSelector(
  selectServicesListPageDomain(),
  (servicesListPage) => servicesListPage
);

export {
  selectServicesListPageDomain,
  getStateServicesListPageFinal,
};