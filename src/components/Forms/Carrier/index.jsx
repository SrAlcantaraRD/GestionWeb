import * as React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { TextField, Button, Paper, Typography } from '@material-ui/core'

const appStyle = theme => ({
  root: {
    width: '90%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
    margin: 'auto',
    marginBottom: theme.spacing.unit
  },
  form: {
    textAlign: 'center',
    padding: '30px',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '45%',
  },
  button: {
    margin: theme.spacing.unit
  }
})

class CarrierForm extends React.Component {
  render() {
    const { firtsName, lastName, phone, nif, error, classes } = this.props
    const isInvalid = firtsName === '' || lastName === '' || phone === '' || nif === ''

    return (
      <Paper className={classes.root}>
        <form onSubmit={this.props.fcnConfirmChanges} className={classes.form}>
          <Typography variant="display1" gutterBottom>
            Datos del Transportista
          </Typography>
          <div>
            <TextField
              id="firtsName"
              label="Nombre"
              className={classes.textField}
              value={firtsName}
              error={!firtsName}
              onChange={this.props.fcnUpdateByPropertyName}
              margin="normal" />
            <TextField
              id="lastName"
              label="Apellidos"
              className={classes.textField}
              value={lastName}
              error={!lastName}
              onChange={this.props.fcnUpdateByPropertyName}
              margin="normal" />
            <TextField
              id="nif"
              label="N.I.F"
              className={classes.textField}
              value={nif}
              error={!nif}
              onChange={this.props.fcnUpdateByPropertyName}
              margin="normal" />
            <TextField
              id="phone"
              label="Telefóno"
              className={classes.textField}
              value={phone}
              error={!phone}
              onChange={this.props.fcnUpdateByPropertyName}
              margin="normal" />
          </div>
          <Button
            variant="contained"
            type="submit"
            color="primary"
            className={classes.button}
            disabled={isInvalid}>
            Guardar
          </Button>
          {error && <p>{error.message}</p>}
        </form>
      </Paper>
    )
  }
}

CarrierForm.propTypes = {
  firtsName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  nif: PropTypes.string.isRequired,
  error: PropTypes.object.isRequired,
  fcnUpdateByPropertyName: PropTypes.func.isRequired,
  fcnConfirmChanges: PropTypes.func.isRequired,
}


export default withStyles(appStyle)(CarrierForm)