import MainCotainer from 'Containers/Main'
import { LandingPage, SignInPage, SignUpPage } from 'Containers/ItemPages'
import PasswordForgetPage from 'Components/PasswordForget'
import { Route, Redirect } from 'react-router-dom'

export const DEFAULT = '*'
export const LANDING = '/'
export const SIGN_UP = '/SignUp'
export const SIGN_IN = '/SignIn'
export const PASSWORD_FORGET = '/RestartPassword'
export const HOME = '/Home'
export const DASHBOARD = '/Dashboard'
export const USER_PROFILE = '/UserProfile'

export const SERVICES_LIST_PAGE = '/Services'
export const CARRIERS_LIST_PAGE = '/Carriers'
export const STORE_LIST_PAGE = '/Stores'

export const NEW_SERVICE = '/NewService'
export const NEW_CARRIER = '/NewCarrier'
export const NEW_STORE = '/NewStore'


const indexRoutesAuth = [
  { type: Route, path: LANDING, component: MainCotainer },
]

const indexRoutesNonAuth = [
  { type: Route, path: LANDING, component: SignInPage },
  // { type: Route, path: LANDING, component: LandingPage },
  // { type: Route, path: SIGN_IN, component: SignInPage },
  // { type: Route, path: SIGN_UP, component: SignUpPage },
  // { type: Route, path: PASSWORD_FORGET, component: PasswordForgetPage },

  // FIXME: Hay que ver cómo hacer el redirect para las paginas que no existen...
  // { type: Redirect, from: DEFAULT, to: LANDING },
]

export {
  indexRoutesAuth,
  indexRoutesNonAuth,
}



