import Constants from './constants'
import { firebase } from 'Firebase'

const firestore = firebase.firestore
const collectionRef = firestore.collection("carriers")

const Actions = {
  onChaneFormInput,
  onSubmit,
  isRequestPending,
  isConfirmingVisible,
}

function isConfirmingVisible(isConfirming = false) {
  return dispatch => {
    dispatch({ type: Constants.CARRIER_PAGE_CHANGE_CONFIRMING, isConfirming })
  }
}

function isRequestPending(isPending = false) {
  return dispatch => {
    dispatch({ type: Constants.CARRIER_PAGE_REQUEST_PENDING, isPending })
  }
}

function onChaneFormInput(property, value) {
  const item = {
    name: property,
    value: value
  }

  return dispatch => {
    dispatch({ type: Constants.CARRIER_PAGE_CHANGE_INPUT, item })
  }
}

function onSubmit(carrier) {
  return dispatch => {

    dispatch(savingData(true))

    collectionRef.add({ ...carrier })
      .then(function () {
        dispatch(success())
      })
      .catch(function (error) {
        dispatch(failure(error))
      });
  }

  function savingData(isPending) {
    return { type: Constants.CARRIER_PAGE_REQUEST_PENDING, isPending}
  }
  function success() {
    return { type: Constants.CARRIER_PAGE_REQUEST_SUCCESS }
  }
  function failure(error) {
    return { type: Constants.CARRIER_PAGE_REQUEST_FAILURE, error }
  }
}

export default Actions
