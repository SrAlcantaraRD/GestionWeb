import React from 'react'
import PropTypes from 'prop-types'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import withMobileDialog from '@material-ui/core/withMobileDialog'
import styled, { keyframes } from 'styled-components'

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

const spinnerDash = keyframes`
  0% {
    stroke-dasharray: 1,200;
    stroke-dashoffset: 0;
  }
  50% {
    stroke-dasharray: 89,200;
    stroke-dashoffset: -35px;
  }
  100% {
    stroke-dasharray: 89,200;
    stroke-dashoffset: -124px;
  }
`

const Svg = styled.svg`
  animation: ${rotate360} 2s linear infinite;
  height: 100%;
  transform-origin: center center;
  width: 100%;
  position: absolute;
  top: 0; bottom: 0; left: 0; right: 0;
  margin: auto;
`

const Spinner = styled.div`
  position: relative;
  margin: 0px auto 10px auto;
  width: ${props => props.spinnerSize};
  width: 25%;
  overflow: 'hidden';
  overflowY: 'hidden';
  overflowX: 'hidden';
  &:before {
    content: '';
    display: block;
    padding-top: 100%;
  }
`
const Circle = styled.circle`
  animation: ${spinnerDash} 1.5s ease-in-out infinite;
  stroke-dasharray: 1,200;
  stroke-dashoffset: 0;
  stroke-linecap: round;
  stroke: ${props => props.color};
`

class AlertLoading extends React.Component {
  fcnOnCancel = (event) => {
    this.props.fcnOnCancel()
    event.preventDefault()
  }

  fcnOnAccept = (event) => {
    this.props.fcnOnAccept()
    event.preventDefault()
  }

  render() {
    const {
      strTitle,
      boolOpen
    } = this.props

    const spinnerNode = (
      <Spinner spinnerSize={this.props.spinnerSize}>
        <Svg viewBox='25 25 50 50'>
          <Circle color='blue' cx='50' cy='50' r='20' fill='none' strokeWidth='2' strokeMiterlimit='10' />
        </Svg>
      </Spinner>
    )

    return (
      <Dialog
        open={boolOpen}
        aria-labelledby="responsive-dialog-title">
        <DialogTitle id="responsive-dialog-title">{strTitle}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {spinnerNode}
          </DialogContentText>
        </DialogContent>
      </Dialog>
    )
  }
}

AlertLoading.propTypes = {
  strTitle: PropTypes.string.isRequired,
  boolOpen: PropTypes.bool.isRequired,
}

AlertLoading.defaultProps = {
  boolOpen: false,
}

export default withMobileDialog()(AlertLoading)