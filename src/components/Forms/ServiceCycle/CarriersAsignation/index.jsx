import * as React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import {
  Button,
  Paper,
  Typography,
  Grid,
  ListSubheader,
  Divider
} from '@material-ui/core'
import CarriersList from './CarriersList'

const appStyle = theme => ({
  root: {
    width: '90%',
  },
  rootPaper: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit,
    maxHeight: 230,
    minHeight: 230,
    overflow: 'auto',
    backgroundColor: theme.palette.background.paper,
  },
})

class CarrierForm extends React.Component {
  constructor(props, context) {
    super(props, context)

    this.onCarrierClick = this.onCarrierClick.bind(this)
    this.state = {
      firtsName: '',
      lastName: '',
      phone: '',
      nif: '',
      key: '',
    }
  }

  componentWillMount() {
    const { carrierList, selectedKey } = this.props
    const selectedCarrier = carrierList.filter(carrier => carrier.key === selectedKey)

    if (selectedCarrier.length > 0) {
      this.setState({ ...selectedCarrier[0] })
    }
  }

  onCarrierClick = (selectedCarrier) => {
    if (this.props.canBeAssigned) {
      this.setState({ ...selectedCarrier })
    }
  }

  onClickHandle = () => {
    const { key } = this.state
    this.props.onAssingCarrier(key)
  }

  render() {
    const { carrierList, classes, canBeAssigned } = this.props
    let { firtsName, lastName, phone, nif } = this.state

    return (
      <Grid container>
        <Grid item xs={4}>
          <CarriersList
            carriersList={carrierList}
            onCarrierClick={this.onCarrierClick}
            canBeAssigned={canBeAssigned} />
        </Grid>
        <Grid item xs={8}>
          <Paper className={classes.rootPaper}>
            <ListSubheader>Detalles</ListSubheader>
            <Divider />
            <Typography variant='display1' >
              {`${firtsName} ${lastName}`}
            </Typography>
            <Typography variant='headline' >
              {`${phone} ${nif}`}
            </Typography>
            <Divider />
            <Button
              variant='contained'
              type='submit'
              color='primary'
              disabled={!firtsName}
              onClick={this.onClickHandle}
              className={classes.button}>
              Asignar
            </Button>
          </Paper>
        </Grid>
      </Grid>
    )
  }
}

CarrierForm.propTypes = {
  selectedKey: PropTypes.string.isRequired,
  carrierList: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
  onAssingCarrier: PropTypes.func.isRequired,
  canBeAssigned: PropTypes.bool.isRequired,
}

CarrierForm.defaultProps = {
  selectedKey: ''
}

export default withStyles(appStyle)(CarrierForm)