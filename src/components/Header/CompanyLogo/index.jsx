import * as React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

const appStyle = theme => ({
  container: {
    float: 'left',
    height: '100%',
  },
  image: {
    borderRadius: '8px',
    height: '60px',
  }
})

class CompanyLogo extends React.Component {
  render() {
    const { classes, logo } = this.props
    return (
      <div className={classes.container}>
        <img src={logo} alt='logo' className={classes.image} />
      </div>
    )
  }
}

CompanyLogo.propTypes = {
  classes: PropTypes.object.isRequired,
  logo: PropTypes.string.isRequired,
}

export default withStyles(appStyle)(CompanyLogo)
