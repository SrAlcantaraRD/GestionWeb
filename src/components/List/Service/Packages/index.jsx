import React from 'react'
import PropTypes from 'prop-types'
import {
  ListSubheader,
  ListItemText,
  withStyles,
  ListItem,
  Divider,
  colors,
  Avatar,
  Paper,
  List,
} from '@material-ui/core'
const styles = theme => ({
  rootPaper: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit,
  },
  rootList: {
    width: '100%',
    maxHeight: 300,
    minHeight: 300,
    overflow: 'auto',
    backgroundColor: theme.palette.background.paper,
  },
  listSection: {
    backgroundColor: 'inherit',
  },
  ul: {
    backgroundColor: 'inherit',
    padding: 0,
  },
  purpleAvatar: {
    color: '#fff',
    backgroundColor: colors.deepPurple[500],
  },

})

function ListServicePackages(props) {
  const { classes, packageList } = props
  return (
    <Paper className={classes.rootPaper}>
      <List dense className={classes.rootList} subheader={<li />}>
        <ul className={classes.ul}>
          <ListSubheader>{`Paquestes`}<Divider /></ListSubheader>
          {
            packageList.map((item, key) =>
              <ListItem key={`ui.${key}`} >
                <Avatar className={classes.purpleAvatar}> {item.quantity}</Avatar>
                <ListItemText primary={item.description} secondary={``} />
              </ListItem>
            )
          }
        </ul>
      </List>
    </Paper>
  )
}

ListServicePackages.propTypes = {
  classes: PropTypes.object.isRequired,
  packageList: PropTypes.array.isRequired,
}

export default withStyles(styles)(ListServicePackages)
