const Constants = {
  USER_PROFILE_CHANGE_INPUT: 'USER_PROFILE_CHANGE_INPUT',
  USER_PROFILE_REQUEST_PENDING: 'USER_PROFILE_REQUEST_PENDING',
  USER_PROFILE_REQUEST_SUCCESS: 'USER_PROFILE_REQUEST_SUCCESS',
  USER_PROFILE_REQUEST_FAILURE: 'USER_PROFILE_REQUEST_FAILURE',
  USER_PROFILE_USER_LOAD:'USER_PROFILE_USER_LOAD',
  USER_PROFILE_CHANGE_CONFIRMING: 'USER_PROFILE_CHANGE_CONFIRMING',
};

export default Constants;
