import { createSelector } from 'reselect';

const selectMainPageDomain = () => state => state.main

const getStateMainPageFinal = () => createSelector(
  selectMainPageDomain(),
  (main) => main
);

export {
  selectMainPageDomain,
  getStateMainPageFinal,
};
