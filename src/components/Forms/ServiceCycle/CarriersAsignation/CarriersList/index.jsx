import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as Icons from '@material-ui/icons'
import {
  ListSubheader,
  ListItemText,
  withStyles,
  ListItem,
  Divider,
  Avatar,
  Paper,
  List,
} from '@material-ui/core'
const styles = theme => ({
  rootPaper: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit,
  },
  rootList: {
    width: '100%',
    maxHeight: 230,
    minHeight: 230,
    overflow: 'auto',
    backgroundColor: theme.palette.background.paper,
  },
  listSection: {
    backgroundColor: 'inherit',
  },
  ul: {
    backgroundColor: 'inherit',
    padding: 0,
  },
})

class ListServiceCarriers extends Component {
  constructor(props) {
    super(props)
    this.onClickHandle = this.onClickHandle.bind(this)
  }

  onClickHandle = carrier => () => {
    this.props.onCarrierClick(carrier)
  }

  render() {

    const { classes, carriersList, canBeAssigned } = this.props
    return (
      <Paper className={classes.rootPaper}>
        <List dense className={classes.rootList} subheader={<li />}>
          <ul className={classes.ul}>
            <ListSubheader>{`Transportistas Activos`}<Divider /></ListSubheader>
            {
              carriersList.filter(carrier => carrier.active).map((carrier, key) =>
                <ListItem button={canBeAssigned} key={`ui.${key}`} onClick={this.onClickHandle(carrier)}>
                  <Avatar>
                    <Icons.Image />
                  </Avatar>
                  <ListItemText primary={`${carrier.firtsName}`} secondary={carrier.phone} />
                </ListItem>
              )
            }
          </ul>
        </List>
      </Paper>
    )
  }
}

ListServiceCarriers.propTypes = {
  classes: PropTypes.object.isRequired,
  carriersList: PropTypes.array.isRequired,
  onCarrierClick: PropTypes.func.isRequired,
  canBeAssigned: PropTypes.bool.isRequired,
}

export default withStyles(styles)(ListServiceCarriers)
