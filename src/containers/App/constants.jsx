const Constants = {
  APP_USER_LOGIN: 'APP_USER_LOGIN',
  APP_USER_NON_LOGIN: 'APP_USER_NON_LOGIN',
};

export default Constants;
