import Constants from './constants';

export const initialState = {
  storeData: {
    companyName: 'companyName'+ new Date().getTime(),
    nif: 'nif'+ new Date().getTime(),
    email: 'email@email.'+ new Date().getTime(),
    phone: 'phone'+ new Date().getTime(),
    contactPerson: 'contactPerson'+ new Date().getTime(),
    city: 'city'+ new Date().getTime(),
    address: 'address'+ new Date().getTime(),
    postalCode: 'postalCode'+ new Date().getTime(),
  },
  error: {},
  isPending: false,
  isConfirming: false,
  isCreated: false,
  isSubmited: false,
};

export default function SingUpPageReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.NEW_STORE_CHANGE_INPUT:
      const newState = state.storeData
      newState[action.item.name] = action.item.value
      return Object.assign({}, state, {storeData: newState})
    case Constants.NEW_STORE_CHANGE_CONFIRMING:
      return Object.assign({}, state, { isConfirming: action.isConfirming })
    case Constants.NEW_STORE_REQUEST_PENDING:
      return Object.assign({}, state, { isPending: action.isPending, error: {}, isConfirming: false })
    case Constants.NEW_STORE_REQUEST_FAILURE:
      return Object.assign({}, state, { error: action.error, isPending: false, isSubmited:true })
    case Constants.NEW_STORE_REQUEST_SUCCESS:
      return Object.assign({}, state, { created: true, isCreated: true, isSubmited: false, isPending: false })
    case Constants.NEW_STORE_RESTART_FORM:
      return Object.assign({}, initialState)
    default:
      return state;
  }
}
