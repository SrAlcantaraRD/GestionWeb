import React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core/styles'

const appStyle = theme => ({
  listWrapper: {
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
    border: '2px solid rgba(255, 255, 255, 0.1)',
    borderRadius: '3px',
    padding: 0,
    margin: 0,
    width: '100%',
  },
  listSimpleWrapper: {
    padding: 0,
    margin: 0,
    width: '100%',
  }
  ,list: {
    listStyle: 'none',
    padding: 0,
    margin: 0,
    width: '100%',
    borderRadius: '2px',
    borderWidth: 0,
    borderColor: 'rgba(255, 255, 255, 0.25)',
    fontWeight: 200,
  },
  empty:  {
    borderColor: '#575756',
    borderWidth: '0 2px',
    color: 'white',
    textAlign: 'center',
    display: 'block',
  }
})

class List extends React.Component {
    render(){
      const { onDeleteAction, onEditAction, ComponentToRender, classes} = this.props
      let content = (<div></div>)

      if (this.props.items && this.props.items.length > 0) {
        content = this.props.items.map((item, index) => (
          <ComponentToRender
            key={`item-${index}`}
            item={item}
            onDeleteAction={onDeleteAction}
            onEditAction={onEditAction}
          />
        ))
      } else {
        // Otherwise render a single component
        content = <span className={classes.empty}>{this.props.emptyMessage}</span>
      }
      return (
        <div className={this.props.simpleWrapper ? classes.simpleWrapper : classes.listWrapper}>
          <ul className={classes.list}>
            {content}
          </ul>
        </div>
      )
    }
}
List.propTypes = {
  simpleWrapper: PropTypes.bool,
  ComponentToRender: PropTypes.func.isRequired,
  onDeleteAction: PropTypes.func,
  onEditAction: PropTypes.func,
}

List.defaultProps = {
  emptyMessage: 'No hay elementos en este listado',
  items: [],
}

export default withStyles(appStyle)(List)
