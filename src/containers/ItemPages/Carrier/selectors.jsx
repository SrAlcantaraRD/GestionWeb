import { createSelector } from 'reselect';

const selectCarrierPageDomain = () => state => state.itemPages.carrier
const getStateCarrierPageFinal = createSelector(
  selectCarrierPageDomain(),
  (carrierPage) => carrierPage
);

export {
  selectCarrierPageDomain,
  getStateCarrierPageFinal,
};