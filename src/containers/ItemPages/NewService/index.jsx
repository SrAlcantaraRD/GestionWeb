import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Actions from './actions'
import { getStateNewServicePageFinal } from './selectors'
import { withStyles } from '@material-ui/core/styles'
import {
  Button,
  Stepper,
  Step,
  StepLabel,
  StepContent,
  Paper,
  Typography,
  Snackbar
} from '@material-ui/core'
import { history } from 'Helpers'
import * as routes from 'Routes'
import {
  Alerts,
  TransmitterForm,
  ReceiverForm,
  PackagesForm,
  ObservationsForm,
  SendServiceForm
} from 'Components'
const styles = theme => ({
  root: {
    width: '90%',
    margin: 'auto',
    marginTop: '20px',
    padding: theme.spacing.unit,
  },
  rootButtons: {
    margin: theme.spacing.unit * 2,
    padding: theme.spacing.unit,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '45%',
  },
  button: {
    marginTop: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  actionsContainer: {
    marginBottom: theme.spacing.unit * 2,
  },
  resetContainer: {
    padding: theme.spacing.unit,
  },
  title: {
    textAlign: 'center',
    paddingTop: '15px',
  }
})

class NewServicePage extends Component {
  constructor(props) {
    super(props)

    this.goToServicesList = this.goToServicesList.bind(this)
    this.getStepDescription = this.getStepDescription.bind(this)
    this.getSteps = this.getSteps.bind(this)
    this.getStepContent = this.getStepContent.bind(this)
    this.getButtons = this.getButtons.bind(this)
    this.updateByPropertyName = this.updateByPropertyName.bind(this)
    this.onUpdatePackages = this.onUpdatePackages.bind(this)
    this.saveService = this.saveService.bind(this)
    this.getBottomButtons = this.getBottomButtons.bind(this)
  }

  getBottomButtons = (showButtons) => {
    const { classes } = this.props
    return (
      showButtons && (
        <Paper square elevation={0} className={classes.resetContainer}>
          <Typography variant="subheading" gutterBottom>Servicio solicitado. Pendiente de asignación</Typography>
          <Button variant="contained" color="primary" onClick={this.handleStep(1)} className={classes.button}>
            Solicitar otro servicio
          </Button>
          <Button color="primary" onClick={this.goToServicesList} className={classes.button}>
            Ver servicios pendientes
          </Button>
        </Paper>
      )
    )
  }

  getButtons = (activeStep, maxStep) => {
    const { classes } = this.props

    const buttonLeft = activeStep !== 0 && (
      <Button
        onClick={this.handleBack}
        className={classes.button}>
        Anterior
      </Button>
    )

    const buttonNext = activeStep !== maxStep && (
      <Button
        variant="contained"
        color="primary"
        onClick={this.handleNext}
        className={classes.button}>
        Siguiente
      </Button>
    )

    const buttonSave = activeStep === maxStep && (
      <Button
        variant="contained"
        color="primary"
        onClick={this.saveService}
        className={classes.button}>
        Solicitar
      </Button>
    )

    return activeStep === maxStep && (
      <div className={classes.actionsContainer}>
        {buttonLeft}
        {buttonNext}
        {buttonSave}
      </div>
    )
  }

  saveService = () => {
    let service = this.props.page

    delete service.transmitter
    delete service.activeStep
    delete service.error

    service.history = []
    service.history.push({
      date: new Date().getTime(),
      user: this.props.page.userKey,
      action: 'create',
    })

    this.props.fcnSaveService(service)
  }

  updateByPropertyName = (event) => {
    const { id, value } = event.target
    this.props.fcnUpdateByPropertyName(id, value)
  }

  onUpdatePackages = (packages) => {
    this.props.fcnUpdateByPropertyName('packages', packages)
  }


  getSteps = () => {
    return ['Datos de Recogida', 'Datos de Entrega', 'Paquetes', 'Confirmacación']
  }

  getStepDescription = (step) => {
    switch (step) {
      case 0:
        return `Compruebe que los datos introducidos son correctos.`
      case 1:
        return 'Introduzca los datos del receptor'
      case 2:
        return `Introduzca los bultos/paquetes que se han de recoger y entregar`
      default:
        return 'Paso no configurado'
    }
  }

  getStepContent(step) {
    const { classes, page } = this.props
    const { transmitter, packages, observations } = page
    switch (step) {
      case 0: return <TransmitterForm {...transmitter} classes={classes} fncNextStep={this.handleNext} />
      case 1: return <ReceiverForm {...page} classes={classes} onChangeInput={this.updateByPropertyName} fncNextStep={this.handleNext} fcnPreviouStep={this.handleBack} />
      case 2: return <PackagesForm packages={packages} fcnUpdatePackages={this.onUpdatePackages} fncNextStep={this.handleNext} fcnPreviouStep={this.handleBack} />
      case 3: return <ObservationsForm observations={observations} fncOnChange={this.updateByPropertyName} />
      case 4: return <SendServiceForm {...page} />
      default:
        return 'Paso no configurado'
    }
  }

  handleNext = () => {
    let { activeStep } = this.props.page
    activeStep++

    this.props.fcnChangeStep(activeStep)
  }

  handleBack = () => {
    let { activeStep } = this.props.page
    activeStep--
    this.props.fcnChangeStep(activeStep)
  }

  goToServicesList = () => {
    this.props.fcnUpdateByPropertyName('activeStep', 0)
    history.push(routes.SERVICES_LIST_PAGE)
  }

  handleClose = () => {
    this.props.fcnCloseSnackbar()
  }

  handleStep = step => () => {
    this.props.fcnUpdateByPropertyName('activeStep', step)
  };

  render() {
    const { classes } = this.props
    const { isPending, isCreated, activeStep } = this.props.page
    const steps = this.getSteps()
    return (
      <Paper className={classes.root}>
        <Snackbar
          anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
          open={isCreated}
          autoHideDuration={6000}
          onClose={this.handleClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">Servicio enviado.</span>}
        />
        <Alerts.Loading
          boolOpen={isPending}
          strTitle={'Guardando datos'} />
        <Typography variant="display1" gutterBottom className={classes.title}>
          Nuevo Servicio
        </Typography>
        <Stepper activeStep={activeStep} orientation="vertical">
          {steps.map((label, index) => {
            return (
              <Step key={label}>
                {/* TODO: con este componente, se puede hacer clic en la sección ¿Es necesaria?
                <StepButton
                  onClick={this.handleStep(index)}
                  completed={true}
                > */}
                <StepLabel>{label}</StepLabel>
                {/* </StepButton> */}
                <StepContent>
                  <Typography>{this.getStepDescription(index)}</Typography>
                  {this.getStepContent(index)}
                  {this.getButtons(index, steps.length - 1)}
                </StepContent>
              </Step>
            )
          })}
        </Stepper>
        <Paper className={classes.rootButtons}>
          {this.getBottomButtons(activeStep === steps.length)}
        </Paper>
      </Paper>
    )
  }
}

NewServicePage.propTypes = {
  classes: PropTypes.object,
  fcnUpdateByPropertyName: PropTypes.func.isRequired,
}

/* STATE SECTION */
function mapStateToProps(state) {
  const page = getStateNewServicePageFinal(state)
  return { page }
}

/* DISPATCH SECTION */
var mapDispatchToProps = (dispatch) => {
  return {
    fcnUpdateByPropertyName: (property, value) => {
      dispatch(Actions.chaneFormInput(property, value))
    },
    fcnSaveService: (service) => {
      dispatch(Actions.addService(service))
    },
    fcnCloseSnackbar: () => {
      dispatch(Actions.closeSnackbar())
    },
    fcnChangeStep: (step) => {
      dispatch(Actions.changeActiveStep(step))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(NewServicePage))