import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Paper, TextField, Button } from '@material-ui/core'

class AddForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      item: {
        description: '',
        quantity: 0
      }
    }

    this.onAddPackage = this.onAddPackage.bind(this)
    this.onPropertyChange = this.onPropertyChange.bind(this)
  }

  onPropertyChange = (event) => {
    const { id, value } = event.target
    let { item } = this.state
    item[id] = value
    this.setState({ item: item })
  }

  onAddPackage = () => {
    const { item } = this.state

    this.setState({ item: { description: '', quantity: 0 } })
    this.props.fcnAddPackage(item)
  }

  render() {
    const { classes, nonEditable } = this.props
    const { description, quantity } = this.state.item

    if (nonEditable) {
      return null
    }

    return (
      <Paper className={classes.root}>
        <TextField
          id="description"
          label="Descripción"
          className={classes.textField}
          margin="normal"
          value={description}
          onChange={event => this.onPropertyChange(event)} />
        <TextField
          id="quantity"
          label="Cantidad"
          className={classes.selectField}
          type="number"
          value={quantity}
          margin="normal"
          onChange={event => this.onPropertyChange(event)} />
        <Button
          variant="contained"
          color="primary"
          disabled={!description || quantity <= 0}
          onClick={event => this.onAddPackage()}>
          Añadir
        </Button>
      </Paper>
    )
  }
}

AddForm.propTypes = {
  classes: PropTypes.object.isRequired,
  fcnAddPackage: PropTypes.func.isRequired,
  nonEditable: PropTypes.bool.isRequired,
}

export default AddForm