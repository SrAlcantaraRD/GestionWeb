import Constants from './constants';

export const initialState = {
  username: '',
  password: '',
  error: '',
  isSubmitted: false,
  isLoading: false,
};

export default function SingInPageReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.SING_IN_REQUEST_PENDING:
      return Object.assign({}, state, { isPending: action.isPending })
    case Constants.SING_IN_FORM_SUBMITTED:
      return Object.assign({}, state, { isSubmitted: action.isSubmitted })
    case Constants.SING_IN_CHANGE_INPUT:
      const newData = []
      newData[action.item.name] = action.item.value
      return Object.assign({}, state, newData)
    case Constants.SING_IN_REQUEST_FAILURE:
      return Object.assign({}, state, { error: action.error, isPending: false })
    default:
      return state;
  }
}
