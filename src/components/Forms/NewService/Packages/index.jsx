import * as React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { Paper, Button } from '@material-ui/core'
import AddForm from './AddForm'
import PackagesTable from './PackagesTable'

const styles = theme => ({
  root: {
    width: '99%',
    marginTop: theme.spacing.unit,
    margin: 'auto',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '60%',
  },
  selectField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '20%',
  },
  actionsContainer: {
    marginTop: theme.spacing.unit * 2,
  },
})

class PackagesForm extends React.Component {
  constructor(props) {
    super(props)

    this.fcnAddPackage = this.fcnAddPackage.bind(this)
    this.fcnUpdatePackageList = this.fcnUpdatePackageList.bind(this)
    this.handleBack = this.handleBack.bind(this)
    this.handleNext = this.handleNext.bind(this)
  }

  handleNext = () => {
    this.props.fncNextStep()
  }

  handleBack = () => {
    this.props.fcnPreviouStep()
  }

  fcnUpdatePackageList = (packageList) => {
    this.props.fcnUpdatePackages(packageList)
  }

  fcnAddPackage = (newPackage) => {
    const { packages } = this.props
    packages.push(newPackage)
    this.props.fcnUpdatePackages(packages)
  }

  showButtons = () => {
    const { classes, packages, nonEditable } = this.props
    return !nonEditable && (
      <div className={classes.actionsContainer}>
      <Button
        onClick={this.handleBack}
        className={classes.button}>
        Anterior
      </Button>
      <Button
        variant="contained"
        color="primary"
        disabled={packages.length === 0}
        onClick={this.handleNext}
        className={classes.button}>
        Siguiente
      </Button>
    </div>
    )
  }

  render() {
    const { classes, packages, nonEditable } = this.props

    return (
      <div>
        <AddForm
          classes={classes}
          nonEditable={nonEditable}
          fcnAddPackage={this.fcnAddPackage} />
        <Paper className={classes.root}>
          <PackagesTable
            packages={packages}
            nonEditable={nonEditable}
            onUpdatePackage={this.fcnUpdatePackageList} />
        </Paper>
        {this.showButtons()}
      </div>
    )
  }
}

PackagesForm.propTypes = {
  classes: PropTypes.object.isRequired,
  packages: PropTypes.array.isRequired,
  fcnUpdatePackages: PropTypes.func.isRequired,
  nonEditable: PropTypes.bool.isRequired, // TODO: REMOVE THIS FUNCIONALITY
  fncNextStep: PropTypes.func,
  fcnPreviouStep: PropTypes.func,
}

PackagesForm.defaultProps = {
  nonEditable: false,
}

export default withStyles(styles)(PackagesForm)
