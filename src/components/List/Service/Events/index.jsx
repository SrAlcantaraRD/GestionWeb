import React from 'react'
import PropTypes from 'prop-types'
import * as Icons from '@material-ui/icons'
import {
  ListSubheader,
  ListItemText,
  withStyles,
  ListItem,
  Divider,
  Avatar,
  Paper,
  List,
} from '@material-ui/core'
const styles = theme => ({
  rootPaper: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit,
  },
  rootList: {
    width: '100%',
    maxHeight: 300,
    minHeight: 300,
    overflow: 'auto',
    backgroundColor: theme.palette.background.paper,
  },
  listSection: {
    backgroundColor: 'inherit',
  },
  ul: {
    backgroundColor: 'inherit',
    padding: 0,
  },
})

function getTitle(action) {
  let strTitle = action

  switch (action) {
    case 'create':
      strTitle = 'Servicio Solicitado'
      break
    case 'assingCarrier':
      strTitle = 'Servicio Asignado'
      break
    default:
      strTitle = action
  }

  return strTitle
}

function getAvatar(action) {
  let iconToRender = null

  switch (action) {
    case 'create':
      iconToRender = <Icons.NoteAdd />
      break
    case 'changeStatus':
      iconToRender = <Icons.Star />
      break
    case 'newIncidence':
      iconToRender = <Icons.Feedback />
      break
    default:
      iconToRender = <Icons.Help />
  }
  return (
    <Avatar>
      {iconToRender}
    </Avatar>
  )
}

function ListServiceHistorial(props) {
  const { classes, historyList } = props

  const order = historyList.sort(function (a, b) { return b.date - a.date })
  const options = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric'
  }

  return (
    <Paper className={classes.rootPaper}>
      <List dense className={classes.rootList} subheader={<li />}>
        <ul className={classes.ul}>
          <ListSubheader>{`Historial`}<Divider /></ListSubheader>
          {
            order.map((event, key) =>
              <ListItem key={`ui.${key}`} >
                {getAvatar(event.action)}
                <ListItemText
                  primary={getTitle(event.action)}
                  secondary={new Date(event.date).toLocaleDateString('es-ES', options)} />
              </ListItem>
            )
          }
        </ul>
      </List>
    </Paper>
  )
}

ListServiceHistorial.propTypes = {
  classes: PropTypes.object.isRequired,
  historyList: PropTypes.array.isRequired,
}

export default withStyles(styles)(ListServiceHistorial)
