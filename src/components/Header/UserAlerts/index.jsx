import * as React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'
import { NotificationImportant } from '@material-ui/icons'

const appStyle = theme => ({
  container: {
    float: 'right',
    height: '100%',
    width: '80px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderLeft: '2px solid #3f51b5'
  }
})

class UserAlerts extends React.Component {
  render() {
    const { classes, onClick, fontSize } = this.props

    return (
      <div className={classes.container} >
        <div className={classes.item}>
          <NotificationImportant color="primary" onClick={onClick} style={{fontSize:fontSize}} />
        </div>
      </div>
    )
  }
}

UserAlerts.propTypes = {
  classes: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  fontSize: PropTypes.number,
}

UserAlerts.defaultProps = {
  fontSize: 40
};

export default withStyles(appStyle)(UserAlerts)
