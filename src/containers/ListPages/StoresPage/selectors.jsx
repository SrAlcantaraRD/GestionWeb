import { createSelector } from 'reselect';

const selectStoresListPageDomain = () => state => state.listPages.stores

const getStateListPageStoresFinal = createSelector(
  selectStoresListPageDomain(),
  (StoresListPageList) => StoresListPageList
);

export {
  selectStoresListPageDomain,
  getStateListPageStoresFinal,
};