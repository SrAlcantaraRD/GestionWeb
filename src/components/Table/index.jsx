import React from 'react'
import PropTypes from 'prop-types'
import { Table as OriginalTable, Column, Cell } from 'fixed-data-table-2'
import 'fixed-data-table-2/dist/fixed-data-table.css'
import { withStyles } from '@material-ui/core/styles'
import Dimensions from 'react-dimensions'
import { Button } from '@material-ui/core'

const appStyle = theme => ({
  wrapperStyles: {
    border: 'none',
    overflow: 'hidden',
  },
  newTableHeader: {
    color: '#000',
    fontSize: '12px',
    lineHeight: '1',
    background: '#CCFFEE',
    border: 'none'
  }
})

class Table extends React.Component {
  longClickTimer = null;

  constructor(props) {
    super(props);

    var dataList = this.props.rows

    this.state = {
      dataList,
      columns: this.getColumns(),
      longPressedRowIndex: -1,
    };
  }

  handleRowMouseDown(rowIndex) {
    this.cancelLongClick()
    this.setState({longPressedRowIndex: rowIndex})
  }

  handleRowMouseUp() {
    this.cancelLongClick();
  }

  cancelLongClick() {
    if (this.longClickTimer) {
      clearTimeout(this.longClickTimer);
      this.longClickTimer = null;
    }
  }

  getColumns() {
    let columnsToRender = [];
    const { classes, columns } = this.props
    Object.keys(columns).forEach(columnKey => {
      columnsToRender.push(
        <Column
          key={columnKey}
          columnKey={columnKey}
          flexGrow={2}
          header={<Cell className={classes.newTableHeader}>{columns[columnKey]}</Cell>}
          cell={cell => this.getCell(cell.rowIndex, columnKey)}
          width={100}
        />);
    });
    return columnsToRender;
  }

  getCell(rowIndex, columnKey) {
    const { rows } = this.props
    const isCellHighlighted = this.state.longPressedRowIndex === rowIndex;

    let rowStyle = {
      backgroundColor: isCellHighlighted ? 'yellow' : 'transparent',
      width: '100%',
      height: '100%'
    }

    return (
      <Cell style={rowStyle}>
        {rows[rowIndex][columnKey]}
      </Cell>
    )
    // return <Cell style={rowStyle}
    //   data={this.state.dataList}
    //   rowIndex={rowIndex}
    //   columnKey={columnKey};
  }

  render() {
    const { classes, rows, containerHeight, containerWidth } = this.props
    const MyCustomCell = ({ mySpecialProp }) =>
      <Cell>
        <Button
          variant="contained"
          type="submit"
          color="primary">
            Stat
        </Button>
      </Cell>
    return (
      <OriginalTable
        className={classes.wrapperStyles}
        rowHeight={50}
        rowsCount={rows.length}
        width={containerWidth}
        height={containerHeight}
        onRowDoubleClick={(event, rowIndex) => { this.handleRowMouseDown(rowIndex); }}
        onRowMouseUp={(event, rowIndex) => { this.handleRowMouseUp(rowIndex); }}
        headerHeight={50}>
        {this.state.columns}
        <Column
          flexGrow={5}
          header={<Cell className={classes.newTableHeader}>Col 2</Cell>}
          cell={<MyCustomCell mySpecialProp="column2" />}
          width={20}
        />
      </OriginalTable>
    )
  }
}

Table.propTypes = {
  classes: PropTypes.object,
  columns: PropTypes.object,
  emptyMessage: PropTypes.string,
  rows: PropTypes.array,
}

Table.defaultProps = {
  emptyMessage: 'No hay elementos en este listado',
  rows: [],
}

export default Dimensions({
  getHeight: function (element) {
    return window.innerHeight - 170
  },
  getWidth: function (element) {
    var widthOffset = window.innerWidth < 960 ? 55 : 320;
    return window.innerWidth - widthOffset
  }
})(withStyles(appStyle)(Table))
