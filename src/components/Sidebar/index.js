import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import classNames from 'classnames';
import {
    withStyles,
    Drawer,
    Hidden,
    List,
    ListItem,
    ListItemIcon,
    ListItemText
} from '@material-ui/core';

import {HeaderLinks} from 'Components';
import sidebarStyle from 'styleJSS/sidebarStyle.jsx';

class Sidebar extends React.Component {
    // verifies if routeName is the one active (in browser input)
    activeRoute(routeName) {
        return this.props.location.pathname.indexOf(routeName) > -1 ? true : false;
    }

    buildBrand(classes, logo, logoText){
        return (
            <div className={classes.logo}>
                <a href='/' className={classes.logoLink}>
                    <div className={classes.logoImage}>
                        <img src={logo} alt='logo' className={classes.img}/>
                    </div>
                    {logoText}
                </a>
            </div>
        );
    }

    buildMenuLinks(classes, routes, color) {
        // TODO: añadir filtrado según permisos
        // TODO: añadir filtro si es menú

        return (<List className={classes.list}>
            {routes.map((prop, key) => {
                if (prop.redirect)
                    return null;
                const listItemClasses = classNames({
                    [' ' + classes[color]]: this.activeRoute(prop.path)
                });
                const whiteFontClasses = classNames({
                    [' ' + classes.whiteFont]: this.activeRoute(prop.path)
                });
                return (
                    <NavLink to={prop.path} className={classes.item} activeClassName='active' key={key}>
                        <ListItem button className={classes.itemLink + listItemClasses}>
                            <ListItemIcon className={classes.itemIcon + whiteFontClasses}>
                                <prop.icon />
                            </ListItemIcon>
                            <ListItemText primary={prop.sidebarName} className={classes.itemText + whiteFontClasses} disableTypography={true} />
                        </ListItem>
                    </NavLink>
                );
            })}
        </List>);
    }

    render() {
        const props = this.props;
        const {
            classes,
            color,
            logo,
            image,
            logoText,
            routes
        } = props;

        var links = this.buildMenuLinks(classes, routes, color);
        var brand = this.buildBrand(classes, logo, logoText);
        return (
            <div>
                <Hidden mdUp>
                    <Drawer
                        variant='temporary'
                        anchor='right'
                        open={props.open}
                        classes={{paper: classes.drawerPaper}}
                        onClose={props.handleDrawerToggle}
                        ModalProps={{keepMounted: true}}>
                        {brand}
                        <div className={classes.sidebarWrapper}>
                            <HeaderLinks/> {links}
                        </div>
                        {image !== undefined ? (<div className={classes.background} style={{backgroundImage: 'url(' + image + ')'}}/> ): null}
                    </Drawer>
                </Hidden>
                <Hidden smDown>
                    <Drawer
                        anchor='left'
                        variant='permanent'
                        open
                        classes={{paper: classes.drawerPaper}}>
                        {brand}
                        <div className={classes.sidebarWrapper}>{links}</div>
                        {image !== undefined ? (
                            <div
                                className={classes.background}
                                style={{backgroundImage: 'url(' + image + ')'}}/>
                        ): null}
                    </Drawer>
                </Hidden>
            </div>
        );
    }
}

Sidebar.propTypes = {
    color: PropTypes.string,
    logo: PropTypes.string,
    image: PropTypes.string,
    logoText: PropTypes.string,
    routes: PropTypes.array,
    location: PropTypes.object,
    classes: PropTypes.object.isRequired,
    handleDrawerToggle:PropTypes.func,
};

export default withStyles(sidebarStyle)(Sidebar);
