import React from 'react';
import PropTypes from 'prop-types';
import {
    withStyles,
    Card,
    CardContent,
    CardHeader,
    Typography,
    Tabs,
    Tab
} from '@material-ui/core';
import {BugReport, Code, Cloud} from '@material-ui/icons';

import {Tasks} from 'Components';

import tasksCardStyle from 'styleJSS/tasksCardStyle';

class TasksCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: 0
        };
    }

    handleChange(event, value) {
        this.setState({value});
    }

    render() {
        const {classes} = this.props;
        const bugs = [
            'Sign contract for "What are conference organizers afraid of?"', 'Lines From Great Russian Literature? Or E-mails From My Boss?', 'Flooded: One year later, assessing what was lost and what was found when a ravag' +
                    'ing rain swept through metro Detroit',
            'Create 4 Invisible User Experiences you Never Knew About'
        ];
        const website = [
            'Flooded: One year later, assessing what was lost and what was found when a ravag' +
                    'ing rain swept through metro Detroit',
            'Sign contract for "What are conference organizers afraid of?"'
        ];
        const server = [
            'Lines From Great Russian Literature? Or E-mails From My Boss?', 'Flooded: One year later, assessing what was lost and what was found when a ravag' +
                    'ing rain swept through metro Detroit',
            'Sign contract for "What are conference organizers afraid of?"'
        ];
        
        return (
            <Card className={classes.card}>
                <CardHeader
                    classes={
                        {
                            root: classes.cardHeader,
                            title: classes.cardTitle,
                            content: classes.cardHeaderContent
                        }
                    }
                    title='Tasks:'
                    action={
                        < Tabs 
                            classes = {{ flexContainer: classes.tabsContainer, indicator: classes.displayNone }}
                            value = {this.state.value}
                            onChange = {this.handleChange.bind(this)}
                            textColor = 'inherit' >
                            <Tab
                                classes={{
                                    wrapper: classes.tabWrapper,
                                    labelIcon: classes.labelIcon,
                                    label: classes.label,
                                    // textColorInheritSelected: classes.textColorInheritSelected
                                }}
                                icon={<BugReport className = {classes.tabIcon} />}
                                label={'Bugs'}/>
                            < Tab 
                                classes = {{ 
                                    wrapper: classes.tabWrapper, 
                                    labelIcon: classes.labelIcon, 
                                    label: classes.label, 
                                    // textColorInheritSelected: classes.textColorInheritSelected 
                                }}
                                icon = { < Code className = {classes.tabIcon} />}
                                label = {'Website'} />
                            <Tab
                                classes={{
                                    wrapper: classes.tabWrapper,
                                    labelIcon: classes.labelIcon,
                                    label: classes.label,
                                    // textColorInheritSelected: classes.textColorInheritSelected
                                }}
                                icon={< Cloud className = {classes.tabIcon} />}
                                label={'Server'}/> 
                        </Tabs>
                    }
                />
                <CardContent>
                    {this.state.value === 0 && (
                        <Typography component='div'>
                            <Tasks checkedIndexes={[0, 3]} tasksIndexes={[0, 1, 2, 3]} tasks={bugs}/>
                        </Typography>
                    )}
                    {this.state.value === 1 && (
                        <Typography component='div'>
                            <Tasks checkedIndexes={[0]} tasksIndexes={[0, 1]} tasks={website}/>
                        </Typography>
                    )}
                    {this.state.value === 2 && (
                        <Typography component='div'>
                            <Tasks checkedIndexes={[1]} tasksIndexes={[0, 1, 2]} tasks={server}/>
                        </Typography>
                    )}
                </CardContent>
            </Card>
        );
    }
}

TasksCard.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(tasksCardStyle)(TasksCard);
