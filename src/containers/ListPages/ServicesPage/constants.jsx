const Constants = {
  SERVICES_LIST_PAGE_PACKAGES_LOADED: 'SERVICES_LIST_PAGE_PACKAGES_LOADED',
  SERVICES_LIST_PAGE_SET_TAB_CONFIGURATION: 'SERVICES_LIST_PAGE_SET_TAB_CONFIGURATION',
  SERVICES_LIST_PAGE_CHANGE_SEARCH_VALUE: 'SERVICES_LIST_PAGE_CHANGE_SEARCH_VALUE',
  SERVICES_LIST_PAGE_REQUEST_FAILURE: 'SERVICES_LIST_PAGE_REQUEST_FAILURE',
  SERVICES_LIST_PAGE_SAVE_SUCCESS: 'SERVICES_LIST_PAGE_SAVE_SUCCESS',
  SERVICES_LIST_PAGE_CHANGE_STATUS_TO_FILTER: 'SERVICES_LIST_PAGE_CHANGE_STATUS_TO_FILTER',
  SERVICES_LIST_PAGE_CHANGE_EXPANDED_KEY: 'SERVICES_LIST_PAGE_CHANGE_EXPANDED_KEY',
};

export default Constants;
