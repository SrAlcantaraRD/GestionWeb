import * as React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { TextField, Button, Paper, Typography } from '@material-ui/core'

const appStyle = theme => ({
  root: {
    width: '90%',
    marginTop: theme.spacing.unit * 2,
    overflowX: 'auto',
    margin: 'auto',
    marginBottom: theme.spacing.unit
  },
  form: {
    textAlign: 'center',
    padding: '30px',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '45%',
  },
  button: {
    margin: theme.spacing.unit
  }
})

class ProfileForm extends React.Component {
  render() {
    const { company, nif, phone, contactPerson, email, error, classes, postalCode, address, city } = this.props
    const isInvalid = company === '' || nif === '' || phone === '' || postalCode === '' || address === '' || city === ''

    // TODO: Make textfiled nonEditable
    // TODO: User could change the password ==> New component and linked is needed
    return (
      <Paper className={classes.root}>
        <form onSubmit={this.props.fcnConfirmChanges} className={classes.form}>
          <Typography variant="display1" gutterBottom>
            Datos Privado
          </Typography>
          <div>
            {/* TODO: Aplicar grid style */}
            <TextField
              id="company"
              label="Compañia"
              className={classes.textField}
              value={company}
              disabled
              margin="normal" />
            <TextField
              id="nif"
              label="N.I.F"
              disabled
              className={classes.textField}
              value={nif}
              margin="normal" />
            <TextField
              id="email"
              label="Correo Electrónico"
              disabled
              className={classes.textField}
              value={email}
              margin="normal" />
            <TextField
              id="phone"
              label="Telefóno"
              className={classes.textField}
              value={phone}
              error={!phone}
              onChange={this.props.fcnUpdateByPropertyName}
              margin="normal" />
            <TextField
              id="contactPerson"
              label="Persona de Contacto"
              className={classes.textField}
              value={contactPerson}
              error={!contactPerson}
              onChange={this.props.fcnUpdateByPropertyName}
              margin="normal" />
            <TextField
              id="city"
              label="Ciudad"
              className={classes.textField}
              value={city}
              error={!city}
              onChange={this.props.fcnUpdateByPropertyName}
              margin="normal" />
            <TextField
              id="address"
              label="Dirección"
              className={classes.textField}
              value={address}
              error={!address}
              onChange={this.props.fcnUpdateByPropertyName}
              margin="normal" />
            <TextField
              id="postalCode"
              label="Codigo Postal"
              className={classes.textField}
              value={postalCode}
              error={!postalCode}
              onChange={this.props.fcnUpdateByPropertyName}
              margin="normal" />
          </div>
          <Button
            variant="contained"
            type="submit"
            color="primary"
            className={classes.button}
            disabled={isInvalid}>
            Guardar Cambios
          </Button>
          {error && <p>{error.message}</p>}
        </form>
      </Paper>
    )
  }
}

ProfileForm.propTypes = {
  company: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  contactPerson: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  postalCode: PropTypes.string.isRequired,
  nif: PropTypes.string.isRequired,
  error: PropTypes.object.isRequired,
  fcnUpdateByPropertyName: PropTypes.func.isRequired,
  fcnConfirmChanges: PropTypes.func.isRequired,
}


export default withStyles(appStyle)(ProfileForm)