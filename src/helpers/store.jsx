
import { createStore, compose, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk' // TODO: ENTENDER CÓMO FUNCIONA
import createReducer from './reducers'

const middlewares = []

middlewares.push(thunkMiddleware)

if (process.env.NODE_ENV === `development`) {
  const { logger } = require(`redux-logger`)
  middlewares.push(logger);
}

const enhancers = [
  applyMiddleware( ...middlewares ),
  window.devToolsExtension ? window.devToolsExtension() : f => f
]

const store = createStore(
  createReducer(),
  compose( 
    ...enhancers
  )
)

export default store
// TODO: Cómo enviar el historial de acciones a un mail