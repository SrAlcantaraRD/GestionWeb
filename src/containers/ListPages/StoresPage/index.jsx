import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withStyles, Typography, IconButton, Menu, Paper, MenuItem, ListItemIcon, ListItemText } from '@material-ui/core'
import { Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core'
import Actions from './actions'
import { getStateListPageStoresFinal } from './selectors'
import { ListItemCarrier } from 'Components'
import { history } from 'Helpers'
import * as Icons from '@material-ui/icons'
import * as routes from 'Routes'

const styles = theme => ({
  root: {
    width: '95%',
    marginTop: theme.spacing.unit * 2,
    margin: 'auto',
  },
  title: {
    textAlign: 'center',
  },
  content: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
    flex: '1 0 auto',
    padding: theme.spacing.unit,
  },
})

class StoresPage extends React.Component {
  constructor(props) {
    super(props)

    this.getColumns = this.getColumns.bind(this)
    this.goToNewStore = this.goToNewStore.bind(this)
    this.onChangeLockState = this.onChangeLockState.bind(this)
    this.state = {
      value: 0,
      anchorEl: null,
    }
  }

  componentWillMount() {
    const { loading } = this.props.page
    if (loading) {
      this.props.fcnLoadStores()
    }
  }

  onChangeLockState = (key, carrier) => {
    this.props.fcnChangeCarrieLock(key, carrier)
  }

  goToNewStore = () => {
    history.push(routes.NEW_STORE)
  }

  handleClose = () => {
    this.setState({ anchorEl: null })
  }

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget })
  }

  getColumns = () => {
    return (
      <TableHead>
        <TableRow>
          <TableCell>Nombre</TableCell>
          <TableCell numeric>Telefono</TableCell>
          <TableCell>NIF</TableCell>
          <TableCell numeric>Activo</TableCell>
          <TableCell></TableCell>
        </TableRow>
      </TableHead>
    )
  }

  render() {
    const { stores } = this.props.page
    const { classes } = this.props
    const { anchorEl } = this.state
    const ITEM_HEIGHT = 48
    return (
      <div className={classes.root}>
        <Typography variant="display1" gutterBottom className={classes.title}>
          Tiendas
          <IconButton
            aria-label="More"
            aria-owns={anchorEl ? 'long-menu' : null}
            aria-haspopup="true"
            onClick={this.handleClick}>
            <Icons.MoreVert />
          </IconButton>
        </Typography>
        <Menu
          id="long-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
          PaperProps={{
            style: {
              maxHeight: ITEM_HEIGHT * 4.5,
              width: 200,
            },
          }}>
          <MenuItem className={classes.menuItem} onClick={this.goToNewStore}>
            <ListItemIcon >
              <Icons.PersonAdd color='primary' />
            </ListItemIcon>
            <ListItemText classes={{ primary: classes.primary }} inset primary="Añadir" />
          </MenuItem>
        </Menu>
        <Paper className={classes.content}>
          <Table >
            {this.getColumns()}
            <TableBody>
              {stores.map((carrier, key) => {
                return (
                  <ListItemCarrier carrier={carrier} key={`key-${key}`} fcnUpdateCarrier={this.onChangeLockState} />
                )
              })}
            </TableBody>
          </Table>
        </Paper>
      </div>
    )
  }
}

StoresPage.propTypes = {
  classes: PropTypes.object.isRequired,
}

/* STATE SECTION */
function mapStateToProps(state) {
  const page = getStateListPageStoresFinal(state)
  return { page }
}

/* DISPATCH SECTION */
var mapDispatchToProps = (dispatch) => {
  return {
    fcnLoadStores: () => {
      dispatch(Actions.loadStores())
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(StoresPage))
