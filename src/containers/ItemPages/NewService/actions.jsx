import Constants from './constants'
import { firebase } from 'Firebase'

const firestore = firebase.firestore
const collectionRef = firestore.collection("services")

const Actions = {
  chaneFormInput,
  addService,
  closeSnackbar,
  changeActiveStep,
}

function changeActiveStep(activeStep) {
  return dispatch => {  
    dispatch({ type: Constants.NEW_SERVICE_CHANGE_STEP, activeStep });
  }
}

function chaneFormInput(property, value) {
  const item = {
    name: property,
    value: value
  }
  return dispatch => {
    dispatch({ type: Constants.NEW_SERVICE_CHANGE_INPUT, item });
  }
}

function closeSnackbar() {
  return dispatch => {
    dispatch({ type: Constants.NEW_SERVICE_CLOSE_SNACKBAR });
  }
}

function addService(service) {
  return dispatch => {

    dispatch(savingData())

    collectionRef.add({ ...service })
      .then(function () {
        dispatch(success())
      })
      .catch(function (error) {
        dispatch(failure(error))
      });
  }

  function savingData() {
    return { type: Constants.NEW_SERVICE_REQUEST_PENDING }
  }
  function success() {
    return { type: Constants.NEW_SERVICE_REQUEST_SUCCESS }
  }
  function failure(error) {
    return { type: Constants.NEW_SERVICE_REQUEST_FAILURE, error }
  }
}
export default Actions