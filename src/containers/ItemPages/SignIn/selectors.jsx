import { createSelector } from 'reselect';

const selectSingInPageDomain = () => state => state.itemPages.singIn
const getStateSingInPageFinal = createSelector(
  selectSingInPageDomain(),
  (singIn) => singIn
);

export {
  selectSingInPageDomain,
  getStateSingInPageFinal,
};