import * as React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { TextField, Button, FormControl, IconButton, Input, InputLabel, InputAdornment, Paper, Typography } from '@material-ui/core'
import { Visibility, VisibilityOff } from '@material-ui/icons/'

const appStyle = theme => ({
  root: {
    width: '90%',
    overflowX: 'auto',
    margin: 'auto',
    marginBottom: theme.spacing.unit
  },
  form: {
    padding: '30px',
    textAlign: 'center',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '45%',
  },
  button: {
    margin: theme.spacing.unit
  }
})

class NewStoreForm extends React.Component {
  state = {
    showPassword: false,
  }

  handleMouseDownPassword = event => {
    event.preventDefault()
  }

  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }))
  }
  render() {
    console.log(this.props)
    const {
      storeData,
      error,
      classes,
      isSubmited,
      isCreated,
      isEditable
    } = this.props
    let isInvalid = false //companyName === '' || nif === '' || email === '' || phone === '' || contactPerson === '' || city === '' || address === '' || postalCode === ''

    return (
      <Paper className={classes.root}>
        <form onSubmit={this.props.confirmRegistration} className={classes.form}>
          <Typography variant="display1" gutterBottom>
            Formulario de Registro
          </Typography>
          <div>
            {
              Object.entries(storeData).map(([key, value]) => {
                isInvalid = isInvalid || !value
                return <TextField
                  id={key}
                  label="Compañia"
                  className={classes.textField}
                  value={value}
                  error={!value && isSubmited}
                  InputProps={{ readOnly: isCreated }}
                  onChange={this.props.fcnUpdateByPropertyName}
                  margin="normal" />
              })
            }
          </div>
          <Button
            variant="contained"
            type="submit"
            color="primary"
            className={classes.button}
            disabled={isInvalid || isCreated}>
            Guardar
        </Button>
          {error && <p>{error.message}</p>}
        </form>
      </Paper>
    )
  }
}

NewStoreForm.propTypes = {
  companyName: PropTypes.string.isRequired,
  nif: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  contactPerson: PropTypes.string.isRequired,
  city: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  postalCode: PropTypes.string.isRequired,
  error: PropTypes.object.isRequired,
  isSubmited: PropTypes.bool.isRequired,
  fcnUpdateByPropertyName: PropTypes.func.isRequired,
  confirmRegistration: PropTypes.func.isRequired,
}


export default withStyles(appStyle)(NewStoreForm)