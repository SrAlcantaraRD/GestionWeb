import Constants from './constants'
import { firebase } from 'Firebase'

const firestore = firebase.firestore
const collectionRef = firestore.collection("stores")

const Actions = {
  loadStores
}

function loadStores(userKey, isAdmin) {
  return dispatch => {
    collectionRef.onSnapshot(function (snapshot) {
      var storesList = []

      snapshot.forEach(function (doc) {
        const store = {
          key: doc.id,
          ...doc.data(),
        }
        storesList.push(store)
      })
      dispatch(success(storesList))
    }, function (error) {
      dispatch(failure(error))
    })
  }

  function success(stores) {
    return { type: Constants.STORES_LIST_PAGE_PACKAGES_LOADED, stores }
  }
  function failure(error) {
    return { type: Constants.STORES_LIST_PAGE_REQUEST_FAILURE, error }
  }
}

export default Actions
