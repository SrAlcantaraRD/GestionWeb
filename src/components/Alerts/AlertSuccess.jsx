import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import withMobileDialog from '@material-ui/core/withMobileDialog'
import Slide from '@material-ui/core/Slide';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class AlertSuccess extends React.Component {

  fcnOnAccept = (event) => {
    this.props.fcnOnAccept()
    event.preventDefault()
  }

  render() {
    const {
      strTitle,
      strDescription,
      strAgree,
      boolOpen
    } = this.props

    return (
      <div>
        <Dialog
          open={boolOpen}
          aria-labelledby="responsive-dialog-title"
          TransitionComponent={Transition}
          aria-describedby="alert-dialog-slide-description">
          <DialogTitle id="responsive-dialog-title">{strTitle}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {strDescription}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.fcnOnAccept} color="primary" autoFocus>
              {strAgree}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

AlertSuccess.propTypes = {
  strTitle: PropTypes.string.isRequired,
  strDescription: PropTypes.string,
  strNonAgree: PropTypes.string.isRequired,
  strAgree: PropTypes.string.isRequired,
  boolOpen: PropTypes.bool.isRequired,
  fcnOnAccept: PropTypes.func.isRequired,
}

AlertSuccess.defaultProps = {
  strNonAgree: 'Cancelar',
  strAgree: 'Aceptar'
}

export default withMobileDialog()(AlertSuccess)