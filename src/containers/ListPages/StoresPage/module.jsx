import Constants from './constants';

export const initialState = {
  stores: [],
  loading: true,
  error: '',
};

export default function ListPageStoresReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.STORES_LIST_PAGE_PACKAGES_LOADED:
      return Object.assign({}, state, { stores: action.stores })
    case Constants.CARRIERS_LIST_PAGE_REQUEST_FAILURE:
      return Object.assign({}, state, { error: action.error })
    default:
      return state;
  }
}
