import { getStateMainPageFinal } from './selectors'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Switch, Route, Redirect } from 'react-router-dom'
// creates a beautiful scrollbar
import { Link } from 'react-router-dom'
import PerfectScrollbar from 'perfect-scrollbar'
import 'perfect-scrollbar/css/perfect-scrollbar.css'
import imgLogo from 'Images/logo.png'
import dashboardRoutes from 'Routes/dashboard'
import { connect } from 'react-redux'
import { Header, Sidebar, HeaderIcon, CompanyLogo, UserAlerts } from 'Components'
import { withStyles, AppBar, Toolbar, Button, Typography } from '@material-ui/core'
import Actions from './actions'
import { history } from 'Helpers'
import * as Icons from '@material-ui/icons'
import * as routes from 'Routes'

const appStyle = theme => ({
  flex: {
    flexGrow: 2,
    margin: theme.spacing.unit,
  },
  content: {
    paddingTop: '75px',
  },
  topBar: {
    paddingTop: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
  }
})

const switchRoutes = (
  <Switch>
    {dashboardRoutes.map((prop, key) => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.to} key={key} />
      return <Route path={prop.path} component={prop.component} key={key} />
    })}
  </Switch>
)

class Main extends Component {
  constructor(props) {
    super(props)
    this.goToUserProfile = this.goToUserProfile.bind(this)
    this.doSignOut = this.doSignOut.bind(this)
    this.goToServiceList = this.goToServiceList.bind(this)
    this.goToCarriersList = this.goToCarriersList.bind(this)
    this.goToStoreList = this.goToStoreList.bind(this)
  }

  componentDidMount(){
    // const ps = new PerfectScrollbar('#content') 
  }

  goToStoreList() {
    history.push(routes.STORE_LIST_PAGE)
  }

  goToCarriersList() {
    history.push(routes.CARRIERS_LIST_PAGE)
  }

  goToServiceList() {
    history.push(routes.SERVICES_LIST_PAGE)
  }

  goToUserProfile = () => {
    history.push(routes.USER_PROFILE)
  }

  doSignOut = () => {
    this.props.fcnDoSingOut()
  }

  render() {
    const {
      classes,
      ...rest
    } = this.props

    const mobileOpen = this.props.mobileOpen

    return (
      <div>
        <AppBar position="fixed" className={classes.topBar}>
          <Toolbar>
            <CompanyLogo logo={imgLogo} />
            <Typography variant="title" color="inherit" className={classes.flex}>
              {history.location.pathname}
            </Typography>
            <HeaderIcon toolTip='Tranportistas' onClick={this.goToCarriersList} componentIcon={Icons.LocalShipping}/>
            <HeaderIcon toolTip='Tiendas' onClick={this.goToStoreList} componentIcon={Icons.Store} />
            <HeaderIcon toolTip='Pedidos' onClick={this.goToServiceList} componentIcon={Icons.Send} />
            <HeaderIcon toolTip='Configuración' onClick={this.goToUserProfile} componentIcon={Icons.Settings} />
            <HeaderIcon toolTip='Desconectar' onClick={this.doSignOut} componentIcon={Icons.SettingsPower} />
          </Toolbar>
        </AppBar>

        <div id="content" className={classes.content}>
          {switchRoutes}
        </div>
        {/* <div className={classes.wrapper}>
          <Sidebar
            routes={dashboardRoutes}
            logoText={'GestiónWeb'}
            logo={imgLogo}
            handleDrawerToggle={this.props.fcnChangeMobileOpen}
            open={mobileOpen}
            color='green'
            {...rest} />
          <div className={classes.mainPanel}>
            <Header
              routes={dashboardRoutes}
              handleDrawerToggle={this.props.fcnChangeMobileOpen}
              {...rest} />
            <div className={classes.content} ref={this.mainPanelRef}>
              <div className={classes.container}>
                {switchRoutes}
              </div>
            </div>
          </div>
        </div> */}
      </div>
    )
  }
}

Main.propTypes = {
  classes: PropTypes.object.isRequired,
  mobileOpen: PropTypes.bool,
  fcnChangeMobileOpen: PropTypes.func.isRequired,
  fcnDoSingOut: PropTypes.func.isRequired
}

/* STATE SECTION */
const mapStateToProps = (state) => {
  const newState = getStateMainPageFinal(state)
  return newState
}

/* DISPATCH SECTION */
var mapDispatchToProps = (dispatch) => {
  return {
    fcnChangeMobileOpen: () => {
      dispatch(Actions.changeMobileOpen())
    },
    fcnDoSingOut: () => {
      dispatch(Actions.logOut())
    }
  }
}

export default withStyles(appStyle)(connect(mapStateToProps, mapDispatchToProps)(Main))
