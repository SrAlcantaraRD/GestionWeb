import Constants from './constants';

export const initialState = {
  services: [],
  loading: true,
  statusToFilter: 0,
  searchValue: '',
  expandedKey: 0,
  tabConfiguration: [
    { status: 0, quantity: 0, description: 'Sin asignación' },
    { status: 1, quantity: 0, description: 'En espera' },
    { status: 2, quantity: 0, description: 'En ruta' },
    { status: 3, quantity: 0, description: 'Entregados' },
  ],
  error: '',
};

export default function ServicesListPageReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.SERVICES_LIST_PAGE_SET_TAB_CONFIGURATION:
      return Object.assign({}, state, { tabConfiguration: action.tabConfiguration })
    case Constants.SERVICES_LIST_PAGE_CHANGE_SEARCH_VALUE:
      return Object.assign({}, state, { searchValue: action.searchValue })
    case Constants.SERVICES_LIST_PAGE_PACKAGES_LOADED:
      return Object.assign({}, state, { services: action.services, loading: false })
    case Constants.SERVICES_LIST_PAGE_REQUEST_FAILURE:
      return Object.assign({}, state, { error: action.error })
    case Constants.SERVICES_LIST_PAGE_CHANGE_STATUS_TO_FILTER:
      return Object.assign({}, state, { statusToFilter: action.statusToFilter })
    case Constants.SERVICES_LIST_PAGE_CHANGE_EXPANDED_KEY:
      return Object.assign({}, state, { expandedKey: action.expandedKey })
    default:
      return state;
  }
}
