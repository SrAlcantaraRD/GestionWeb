import { createSelector } from 'reselect';

const selectNewServicePageDomain = () => state => {
  const transmitter = {
    name: state.itemPages.userProfile.company,
    phone: state.itemPages.userProfile.phone,
    nif: state.itemPages.userProfile.nif,
    city: state.itemPages.userProfile.city,
    address: state.itemPages.userProfile.address,
    contactPerson: state.itemPages.userProfile.contactPerson,
  }

  return {
    transmitter,
    userKey: state.app.key,
    ...state.itemPages.newService
  }
}

const getStateNewServicePageFinal = createSelector(
  selectNewServicePageDomain(),
  (NewServicePage) => NewServicePage
);

export {
  selectNewServicePageDomain,
  getStateNewServicePageFinal,
};