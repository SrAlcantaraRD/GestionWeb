import Constants from './constants'
import UserConstants from '../ItemPages/UserProfile/constants'
import CarriersConstants from '../ListPages/CarriersPage/constants'
import { auth, firebase } from 'Firebase'

const firestore = firebase.firestore
const usersCollectionRef = firestore.collection("users")
const carriersCollectionRef = firestore.collection("carriers")

const Actions = {
  nonLogIn,
  logIn,
}

function logIn(userData) {
  const user = {
    user: userData.email,
    key: userData.uid,
    verified: userData.emailVerified,
    userLogged: true,
    loading: false,
  }

  var userRef = usersCollectionRef.doc(user.key)

  return dispatch => {
    // Load and save user data
    userRef.get().then(function (doc) {
      if (doc.exists) {
        const userData = doc.data()
        user.isAdmin = userData.type === 'admin'
        dispatch(saveAppData(user))
        dispatch(successUserData(userData))
      } else {
        // TODO: Show a message to contact with admin user
        auth.doSignOut()
        dispatch(failure('DOC NOT EXIST'))
      }
    }).catch(function (error) {
      dispatch(failure(error))
    })

    // Load carries list
    carriersCollectionRef.onSnapshot(function (snapshot) {
      var carriers = [];
      snapshot.forEach(function (doc) {
        const carrier = {
          key: doc.id,
          ...doc.data()
        }
        carriers.push(carrier);
      });
      dispatch(successCarriers(carriers))
    }, function (error) {
      dispatch(failure(error))
    });

  }

  function successCarriers(carriers) {
    return { type: CarriersConstants.CARRIERS_LIST_PAGE_PACKAGES_LOADED, carriers }
  }
  function successUserData(userData) {
    return { type: UserConstants.USER_PROFILE_USER_LOAD, userData }
  }
  function failure(error) {
    return { type: UserConstants.USER_PROFILE_REQUEST_FAILURE, error }
  }
  function saveAppData(user) {
    return { type: Constants.APP_USER_LOGIN, user }
  }
}

function nonLogIn() {
  return dispatch => {
    dispatch({ type: Constants.APP_USER_NON_LOGIN })
  }
}

export default Actions