// import React, { Component } from 'react';
// import { Link } from 'react-router-dom';

// import { auth } from 'Firebase';
// import * as routes from 'Routes';
// import {withStyles} from '@material-ui/core/styles';
// import {TextField, Button} from '@material-ui/core';
// import {compose} from 'recompose'
// import {observer} from 'mobx-react';

// const styles = theme => ({
//     textField: {
//         marginLeft: theme.spacing.unit,
//         marginRight: theme.spacing.unit,
//         width: '100%'
//     },
//     button: {
//         margin: theme.spacing.unit
//     }
// });

// const PasswordForgetPage = (classes) =>
//   <div>
//     <h1>PasswordForget</h1>
//     <PasswordForgetForm classes={classes} />
//   </div>

// const updateByPropertyName = (propertyName, value) => () => ({
//   [propertyName]: value,
// });

// const INITIAL_STATE = {
//   email: '',
//   error: null,
// };

// class PasswordForgetForm extends Component {
//   constructor(props) {
//     super(props);

//     this.state = { ...INITIAL_STATE };
//   }

//   onSubmit = (event) => {
//     const { email } = this.state;

//     auth.doPasswordReset(email)
//       .then(() => {
//         this.setState(() => ({ ...INITIAL_STATE }));
//       })
//       .catch(error => {
//         this.setState(updateByPropertyName('error', error));
//       });

//     event.preventDefault();
//   }

//   render() {
//     const {
//       email,
//       error,
//     } = this.state;

//     const isInvalid = email === '';
//     const {classes} = this.props;

//     return (
//       <form onSubmit={this.onSubmit}>
//         <div>
//             <TextField
//                 id="name"
//                 label="Email Address"
//                 className={classes.textField}
//                 value={this.state.email}
//                 onChange={event => this.setState(updateByPropertyName('email', event.target.value))}
//                 margin="normal"/>
//         </div>
//         <Button variant="contained" type="submit" color="primary" className={classes.button} disabled={isInvalid} >
//             Restaurar
//         </Button>
//         { error && <p>{error.message}</p> }
//       </form>
//     );
//   }
// }

// const PasswordForgetLink = () =>
//   <p>
//     <Link to={routes.PASSWORD_FORGET}>Forgot Password?</Link>
//   </p>

// export default compose(withStyles(styles), observer)(PasswordForgetPage);

// export {
//   PasswordForgetForm,
//   PasswordForgetLink,
// };
