import * as React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { TextField, Button, Paper, Typography } from '@material-ui/core'

const appStyle = theme => ({
  root: {
    width: '70%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
    margin: 'auto',
    marginBottom: theme.spacing.unit
  },
  form: {
    textAlign: 'center',
    padding: '30px',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '60%',
  },
  button: {
    margin: theme.spacing.unit
  }
})

class LogInForm extends React.Component {
  render() {
    const { classes, isSubmitted, isInvalid, username, password, error } = this.props

    return (
      <Paper className={classes.root}>
        <form onSubmit={this.props.onSubmit} className={classes.form}>
          <Typography variant="display1" gutterBottom>
            Identificación
          </Typography>
          <div
            className={'form-group' + (isSubmitted && !username ? ' has-error' : '')}>
            <TextField
              id="username"
              label="Usuario"
              className={classes.textField}
              value={username}
              onChange={this.props.onChangeInput}
              margin="normal" />
            {
              isSubmitted && !username &&
              <div className="help-block">
                Introduzca el usuario
              </div>
            }
          </div>
          <div className={'form-group' + (isSubmitted && !password ? ' has-error' : '')}>
            <TextField
              id="password"
              label="Password"
              className={classes.textField}
              type="password"
              autoComplete="current-password"
              value={password}
              onChange={this.props.onChangeInput}
              margin="normal" />
            {isSubmitted && !password && <div className="help-block">Introduzca la contraseña</div>}
          </div>
          <div className="form-group">
            <Button
              variant="contained"
              type="submit"
              color="primary"
              className={classes.button}
              disabled={isInvalid}>
              Acceder
          </Button>
          </div>
          {error && <p>{error.message}</p>}
        </form>
      </Paper>
    )
  }
}

LogInForm.propTypes = {
  classes: PropTypes.object.isRequired,
  onChangeInput: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  isSubmitted: PropTypes.bool.isRequired,
  isInvalid: PropTypes.bool.isRequired,
  username: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  error: PropTypes.string,
}


export default withStyles(appStyle)(LogInForm)