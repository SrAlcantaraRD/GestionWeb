import Constants from './constants';

export const initialState = {
  mobileOpen: false
};

export default function MainReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.MAIN_PAGE_CHANGE_MOBIL_OPEN:
      return { ...state, mobileOpen: !state.mobileOpen};

    default:
      return state;
  }
}
