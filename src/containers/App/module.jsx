import Constants from './constants';

export const initialState = {
  user: '',
  key: '',
  verified: false,
  userLogged: false,
  loading: true,
  isAdmin: false,
};

export default function appReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.APP_USER_LOGIN:
      return Object.assign({}, state, action.user)
    case Constants.APP_USER_NON_LOGIN:
      return Object.assign({}, initialState, { loading: false })
    default:
      return state;
  }
}
