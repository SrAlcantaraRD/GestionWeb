import React, { Component } from 'react'
import { hot } from 'react-hot-loader'
import { connect } from 'react-redux'
import { history } from 'Helpers'
import { indexRoutesAuth, indexRoutesNonAuth } from 'Routes'
import { firebase } from 'Firebase'
import { Alerts } from 'Components'
import Actions from './actions'

class App extends Component {
  constructor(props) {
    super(props)
    //TODO: FUNCIÓN INTERESANTE. Te permite realizar una acción cada vez que cambia la ruta
    history.listen((location, action) => {
      // clear alert on location change
      // this.props.dispatch(AlertActions.clear())
    })
  }

  componentDidMount() {
    firebase.firebase.auth().onAuthStateChanged(authUser => {
      authUser
        ? this.props.dispatch(Actions.logIn(authUser))
        : this.props.dispatch(Actions.nonLogIn())
    });
  }

  render() {
    const { userLogged, loading } = this.props.app
    if (loading) {
      return (
        <div>
          <Alerts.Loading
            boolOpen={true}
            strTitle={'Cargando Sección'} />
        </div>
      )
    }
    const routes = userLogged ? indexRoutesAuth : indexRoutesNonAuth

    return (
      routes.map((prop, key) => {
        return <prop.type {...prop} key={key} />
      })
    )
  }
}

function mapStateToProps(state) {
  const { app } = state
  return { app }
}

export default connect(mapStateToProps)(hot(module)(App))
