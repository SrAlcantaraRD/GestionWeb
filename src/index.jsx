import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Router, Switch } from 'react-router-dom'
import App from 'Containers/App'
import { store, history } from './helpers'
import registerServiceWorker from './registerServiceWorker'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#388e3c',
    },
    accent: {
      main: '#d50000',
    },
  },
})

ReactDOM.render(
  <MuiThemeProvider theme={theme} sheetsManager={new Map()}>
    <Provider store={store}>
      <Router history={history}>
        <Switch>
          <App />
        </Switch>
      </Router>
    </Provider>
  </MuiThemeProvider>
  , document.getElementById('root'))

registerServiceWorker()