import Constants from './constants';

export const initialState = {
  company: '',
  phone: '',
  contactName: '',
  nif: '',
  email: '',
  address: '',
  postalCode: '',
  contactPerson: '',
  city: '',
  error: {},
  isPending: false,
  isConfirming: false,
  isUpdated: false,
  userExist: true,
};

export default function UserProfileReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.USER_PROFILE_CHANGE_INPUT:
      const newData = []
      newData[action.item.name] = action.item.value
      return Object.assign({}, state, newData)
    case Constants.USER_PROFILE_CHANGE_CONFIRMING:
      return Object.assign({}, state, { isConfirming: action.isConfirming })
    case Constants.USER_PROFILE_REQUEST_PENDING:
      return Object.assign({}, state, { isPending: action.isPending, error: {}, isConfirming: false })
    case Constants.USER_PROFILE_REQUEST_FAILURE:
      return Object.assign({}, state, { error: action.error, isPending: false })
    case Constants.USER_PROFILE_USER_LOAD:
      return Object.assign({}, initialState, { ...action.userData })
    case Constants.USER_PROFILE_REQUEST_SUCCESS:
      return Object.assign({}, state, { isPending: false, isUpdated: true })
    default:
      return state;
  }
}
