import LandingPage from './Landing'
import SignInPage from './SignIn'

export {
  LandingPage,
  SignInPage,
}