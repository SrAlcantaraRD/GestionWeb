import React, { Component } from 'react';
import PropTypes from 'prop-types'
import TextField from '@material-ui/core/TextField'
class ObservationsForm extends Component {
  render() {
    const { observations, fncOnChange } = this.props
    return (
      <TextField
        id="observations"
        label="¿Alguna observacion a tener en cuenta?"
        placeholder="Lo estais haciendo bien"
        multiline
        fullWidth
        value={observations}
        onChange={(event) => fncOnChange(event)}
        margin="normal" />
    )
  }
}

ObservationsForm.propTypes = {
  observations: PropTypes.string.isRequired,
  fncOnChange: PropTypes.func.isRequired,
}

export default ObservationsForm