import React from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import Actions from './actions'
import {getStateSingInPageFinal} from './selectors'
import {Alerts,LogInForm} from 'Components'

class SignInPage extends React.Component {
  constructor(props) {
    super(props)

    this.handleChange = this.handleChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.updateByPropertyName = this.updateByPropertyName.bind(this)
  }

  updateByPropertyName(event) {
    const {id, value } = event.target
    this.props.fcnUpdateByPropertyName(id, value)
  }

  handleChange(event) {
    const {name, value} = event.target
    const {user} = this.props.page
    this.setState({
      user: {
        ...user,
        [name]: value
      }
    })
  }

  onSubmit(event) {
    event.preventDefault()
    const {username, password} = this.props.page
    this.props.fcnLogin(username,password)
  }

  render() {
    const {classes} = this.props
    const {isPending, password, isSubmitted, username, error} = this.props.page
    const isInvalid = password === '' || username === ''
    // TODO: username, password CANNOT SAVE AT STORE
    return (
      <div style={{paddingTop: '100px'}}>
        <Alerts.Loading 
          boolOpen={isPending}
          strTitle={'Verificando credenciales'}/>
        <LogInForm 
          classes={classes}
          onSubmit={this.onSubmit}
          onChangeInput={this.updateByPropertyName}
          isSubmitted={isSubmitted}
          isPending={isPending}
          isInvalid={isInvalid}
          password={password}
          username={username}
          error={error}/>
      </div>
    )
  }
}

SignInPage.propTypes = {
  fcnUpdateByPropertyName: PropTypes.func.isRequired,
  fcnLogin: PropTypes.func.isRequired,
  page: PropTypes.shape({
    submitted: PropTypes.bool,
    loading: PropTypes.bool,
    user: PropTypes.shape({
      username: PropTypes.string,
      password: PropTypes.string
    })
  })
}

/* STATE SECTION */
function mapStateToProps(state) {
  const page = getStateSingInPageFinal(state)
  return {page}
}

/* DISPATCH SECTION */
var mapDispatchToProps = (dispatch) => {
  return {
    fcnUpdateByPropertyName: (property, value) => {
      dispatch(Actions.onChaneFormInput(property, value))
    },
    fcnLogin: (username, password) => {
      dispatch(Actions.isRequestPending(true))
      dispatch(Actions.onLogin(username, password))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignInPage)