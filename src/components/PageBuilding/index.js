import React from 'react';
import logo from './logo.svg';
import './App.css';

class PageBuilding extends React.Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">Página en construcción</h1>
                </header>
            </div>
        );
    }
}

export default PageBuilding;