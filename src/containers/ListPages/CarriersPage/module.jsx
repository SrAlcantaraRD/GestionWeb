import Constants from './constants';

export const initialState = {
  carriers: [],
  error: '',
};

export default function CarriersListPageReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.CARRIERS_LIST_PAGE_PACKAGES_LOADED:
      return Object.assign({}, state, { carriers: action.carriers })
    case Constants.CARRIERS_LIST_PAGE_REQUEST_FAILURE:
      return Object.assign({}, state, { error: action.error })
    default:
      return state;
  }
}
