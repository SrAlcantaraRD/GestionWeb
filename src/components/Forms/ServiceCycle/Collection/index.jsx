import * as React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import {
  TextField,
  Button,
  Paper,
  Grid,
  ListSubheader,
  Divider,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
} from '@material-ui/core'

const appStyle = theme => ({
  root: {
    width: '90%',
  },
  rootPaper: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit,
    maxHeight: 230,
    minHeight: 230,
    overflow: 'auto',
    backgroundColor: theme.palette.background.paper,
  },
  formControl: {
    padding: theme.spacing.unit * 2,
  }
})

class CollectionForm extends React.Component {
  constructor(props) {
    super(props)
    this.onClickHandle = this.onClickHandle.bind(this)

    this.state = {
      value: '',
    }
  }

  onClickHandle = () => {
    this.props.onServiceCollected(this.state.value)
  }

  handleChange = event => {
    this.setState({ value: event.target.value })
  }

  render() {
    const { classes, assignedCarrier } = this.props
    const { value } = this.state
    let { firtsName, lastName, phone } = assignedCarrier

    return (
      <Grid container>
        <Grid item xs={4}>
          <Paper className={classes.rootPaper}>
            <ListSubheader>Transportista</ListSubheader>
            <Divider />
            <FormControl component='fieldset' className={classes.formControl}>
              <TextField
                id='name'
                label='Nombre y Apellidos'
                className={classes.textField}
                value={`${firtsName} ${lastName}`}
                type='text'
                margin='normal'
                InputProps={{
                  readOnly: true,
                }}
              />
              <TextField
                id='phone'
                label='Telefono'
                className={classes.textField}
                value={`${phone}`}
                type='text'
                margin='normal'
                InputProps={{
                  readOnly: true,
                }}
              />
            </FormControl>
          </Paper>
        </Grid>
        <Grid item xs={8}>
          <Paper className={classes.rootPaper}>
            <ListSubheader>¿Lugar de recogida?</ListSubheader>
            <Divider />
            <FormControl component='fieldset' required className={classes.formControl}>
              <RadioGroup
                aria-label='Gender'
                name='gender1'
                className={classes.group}
                value={value}
                onChange={this.handleChange}
              >
                <FormControlLabel value='clientLocal' control={<Radio />} label='Local del cliente' />
                <FormControlLabel value='wareHouse' control={<Radio />} label='Almacen' />
              </RadioGroup>
              <Button
                variant='contained'
                type='submit'
                color='primary'
                disabled={!value}
                onClick={this.onClickHandle}
                className={classes.button}>
                Confirmar recogida
            </Button>
            </FormControl>
          </Paper>
        </Grid>
      </Grid>
    )
  }
}

CollectionForm.propTypes = {
  selectedKey: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  onServiceCollected: PropTypes.func.isRequired,
}

export default withStyles(appStyle)(CollectionForm)