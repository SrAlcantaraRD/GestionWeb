import Constants from './constants';

export const initialState = {
  activeStep: 0,
  clientName: '',
  phone: '',
  nif: '',
  city: '',
  address: '',
  deliveryNote: '',
  observations: '',
  isPending: false,
  isCreated: false,
  status: 0,
  error: '',
  history:{},
  packages: [],
};

export default function NewServicePageReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.NEW_SERVICE_CHANGE_INPUT:
      const newData = []
      newData[action.item.name] = action.item.value
      return Object.assign({}, state, newData)
    case Constants.NEW_SERVICE_REQUEST_PENDING:
      return Object.assign({}, state, { isPending: true })
    case Constants.NEW_SERVICE_REQUEST_FAILURE:
      return Object.assign({}, state, { error: action.error, isPending: false })
    case Constants.NEW_SERVICE_REQUEST_SUCCESS:
      return Object.assign({}, initialState, {
        isPending: false,
        isCreated: true,
        packages: [],
        activeStep: state.activeStep + 1
      })
    case Constants.NEW_SERVICE_CLOSE_SNACKBAR:
      return Object.assign({}, state, { isCreated: false })
    case Constants.NEW_SERVICE_CHANGE_STEP:
      return Object.assign({}, state, { activeStep: action.activeStep })
    default:
      return state;
  }
}
