import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import appReducer from 'Containers/App/module'
import mainReducer from 'Containers/Main/module'
import singInReducer from 'Containers/ItemPages/SignIn/module'
import userProfileReducer from 'Containers/ItemPages/UserProfile/module'
import carrierPageReducer from 'Containers/ItemPages/Carrier/module'

// Create entity Pages
import newStorePageReducer from 'Containers/ItemPages/NewStore/module'
import newServicePageReducer from 'Containers/ItemPages/NewService/module'

// List entitys Page
import listPageServicesReducer from 'Containers/ListPages/ServicesPage/module'
import listPageCarriersReducer from 'Containers/ListPages/CarriersPage/module'
import listPageStoresReducer from 'Containers/ListPages/StoresPage/module'

const routing = routerReducer()

const listPagesReducers = combineReducers({
  services: listPageServicesReducer,
  carriers: listPageCarriersReducer,
  stores: listPageStoresReducer,
})

// TODO: don't combine singInReducer when user is logged and vice versa
const itemPagesReducers = combineReducers({
  singIn: singInReducer,
  userProfile: userProfileReducer,
  newService: newServicePageReducer,
  newStore: newStorePageReducer,
  carrier: carrierPageReducer,
})

export default function createReducer() {
  return (state, action) => combineReducers({
    app: appReducer,
    main: mainReducer,
    listPages: listPagesReducers,
    itemPages: itemPagesReducers,
    routing,
  })(action.type === 'APP_USER_LOGOUT' ? undefined : state, action);
}
