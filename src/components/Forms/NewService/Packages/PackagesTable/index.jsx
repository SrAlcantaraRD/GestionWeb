import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Table, TableBody, TableCell, TableHead, TableRow, IconButton } from '@material-ui/core'
import * as Icons from '@material-ui/icons'

class PackagesTable extends Component {
  constructor(props) {
    super(props)

    this.onIncrease = this.onIncrease.bind(this)
    this.onDecrease = this.onDecrease.bind(this)
    this.onDelete = this.onDelete.bind(this)
    this.getActions = this.getActions.bind(this)
    this.getColumns = this.getColumns.bind(this)
  }

  getColumns = () => {
    const { nonEditable } = this.props

    if (nonEditable) {
      return (
        <TableHead>
          <TableRow>
            <TableCell>Descripción</TableCell>
            <TableCell numeric>Cantidad</TableCell>
          </TableRow>
        </TableHead>
      )
    }

    return (
      <TableHead>
        <TableRow>
          <TableCell>Descripción</TableCell>
          <TableCell numeric>Cantidad</TableCell>
          <TableCell></TableCell>
        </TableRow>
      </TableHead>
    )
  }
  
  getActions = (key, item) => {
    const { nonEditable } = this.props
    if (nonEditable) {
      return null
    }
    return (
      <TableCell numeric>
        <IconButton aria-label="Add">
          <Icons.Add color="primary" onClick={event => this.onIncrease(key, item)} />
        </IconButton>
        <IconButton aria-label="Remove" >
          <Icons.Remove color="secondary" onClick={event => this.onDecrease(key, item)} />
        </IconButton>
        <IconButton aria-label="action">
          <Icons.Delete onClick={event => this.onDelete(key)} />
        </IconButton>
      </TableCell>
    )
  }

  onDecrease = (position, item) => {
    item.quantity--

    const { packages } = this.props
    packages[position] = item

    this.props.onUpdatePackage(packages)
  }

  onIncrease = (position, item) => {
    item.quantity++

    const { packages } = this.props
    packages[position] = item

    this.props.onUpdatePackage(packages)
  }

  onDelete = (position) => {
    const { packages } = this.props

    packages.splice(position, 1)

    this.props.onUpdatePackage(packages)
  }

  render() {
    const { packages } = this.props
    if (packages.length === 0) {
      return null
    } else {
      return (
        <Table >
          {this.getColumns()}
          <TableBody>
            {packages.map((item, key) => {
              return (
                <TableRow key={key} hover>
                  <TableCell component="th" scope="row">
                    {item.description}
                  </TableCell>
                  <TableCell numeric>{item.quantity}</TableCell>
                  {this.getActions(key, item)}
                </TableRow>
              )
            })}
          </TableBody>
        </Table>
      )
    }
  }
}

PackagesTable.propTypes = {
  packages: PropTypes.array.isRequired,
  onUpdatePackage: PropTypes.func.isRequired,
  nonEditable: PropTypes.bool.isRequired,
}

export default PackagesTable