import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  withStyles,
  Typography,
  IconButton,
  Menu,
  MenuItem,
  ListItemIcon,
  ListItemText,
  Tabs,
  Input,
  FormControl,
  FormHelperText,
  InputAdornment,
  Tab,
  Badge,
} from '@material-ui/core'
import Actions from './actions'
import { getStateServicesListPageFinal } from './selectors'
import { Alerts, ListItemService } from 'Components'
import { history } from 'Helpers'
import * as Icons from '@material-ui/icons'
import * as routes from 'Routes'

const styles = theme => ({
  root: {
    width: '95%',
    marginTop: theme.spacing.unit * 2,
    margin: 'auto',
  },
  rootTab: {
    marginBottom: theme.spacing.unit,
  },
  title: {
    textAlign: 'center',
  },
  padding: {
    padding: `0 ${theme.spacing.unit * 2}px`,
  },
  margin: {
    margin: theme.spacing.unit * 2,
  },
  searchButton: {
    maxWidth: '200px !important',
    margin: `0 ${theme.spacing.unit * 2}px`,
  }
})

class ServiceListPage extends React.Component {
  constructor(props) {
    super(props)

    this.goToNewService = this.goToNewService.bind(this)
    this.onUpdatService = this.onUpdatService.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleClick = this.handleClick.bind(this)
    this.filterServices = this.filterServices.bind(this)
    this.onChangeSearchValue = this.onChangeSearchValue.bind(this)
    this.onClearSearchValue = this.onClearSearchValue.bind(this)
    this.handleChangeFilterStatus = this.handleChangeFilterStatus.bind(this)
    this.onItemExpand = this.onItemExpand.bind(this)
    this.state = {
      value: 0,
      anchorEl: null,
    }
  }
  
  componentWillMount() {
    const { loading, userKey, isAdmin } = this.props.page
    if (loading) {
      this.props.fcnLoadServices(userKey, isAdmin)
    }
  }

  onChangeSearchValue = event => {
    const searchValue = event.target.value.trim().toUpperCase()
    this.props.fcnChangeSearchValue(searchValue)
  }

  onClearSearchValue = () => {
    this.props.fcnChangeSearchValue('')
  }

  onItemExpand = expandedKey => {
    this.props.fcnChangeExpanendItem(expandedKey)
  }

  onUpdatService = (entitiesKeys, action) => {
    const { serviceKey, updatedFields } = entitiesKeys

    const event = {
      date: new Date().getTime(),
      user: this.props.page.userKey,
      action: action,
    }

    updatedFields.history.push(event)

    this.props.fcnAssingCarrierToServe(serviceKey, updatedFields)
  }

  handleClose = () => {
    this.setState({ anchorEl: null })
  }

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget })
  }

  goToNewService = () => {
    history.push(routes.NEW_SERVICE)
  }

  handleChangeFilterStatus = status => () => {
    this.props.fcnChangeStatusFiltered(status)
  }

  filterServices = () => {
    const { statusToFilter, searchValue } = this.props.page
    return this.props.page.services[statusToFilter].filter(
      item => item.deliveryNote.indexOf(searchValue) > -1
    )
  }

  render() {
    const { loading, carriers, tabConfiguration, statusToFilter, searchValue, expandedKey } = this.props.page

    if (loading) {
      return (
        <div>
          <Alerts.Loading
            boolOpen={true}
            strTitle={'Cargando listado'} />
        </div>
      )
    }

    const { classes } = this.props
    const { anchorEl } = this.state
    const ITEM_HEIGHT = 48
    const servicesList = this.filterServices()

    return (
      <div className={classes.root}>
        <Typography variant="display1" gutterBottom className={classes.title}>
          Servicios
          <IconButton
            aria-label="More"
            aria-owns={anchorEl ? 'long-menu' : null}
            aria-haspopup="true"
            onClick={this.handleClick}>
            <Icons.MoreVert />
          </IconButton>

          {/* TODO: Use react-select to build a beutifil search component */}
          <FormControl
            className={classes.searchButton}
            aria-describedby="weight-helper-text">
            <Input
              id='searchValue'
              type={'text'}
              value={searchValue}
              label='Buscar por albarán'
              onChange={this.onChangeSearchValue}
              endAdornment={searchValue &&
                <InputAdornment position="start">
                  <IconButton
                    aria-label="Toggle password visibility"
                    onClick={this.onClearSearchValue}
                  // onMouseDown={this.handleMouseDownPassword}
                  >
                    <Icons.Cancel />
                  </IconButton>
                </InputAdornment>
              } />
            <FormHelperText id="weight-helper-text">Buscar por albarán</FormHelperText>
          </FormControl>
        </Typography>
        <Tabs
          fullWidth
          className={classes.rootTab}
          value={statusToFilter}
          indicatorColor='primary'
          textColor='primary'>
          {
            tabConfiguration.map((item, key) =>
              <Tab
                onClick={this.handleChangeFilterStatus(item.status)}
                className={classes.padding}
                label={
                  <Badge
                    className={classes.padding}
                    color={item.quantity > 0 ? 'primary' : 'secondary'}
                    badgeContent={item.quantity}>
                    {item.description}
                  </Badge>
                }
              />
            )
          }


          {/* Botón bonito para un enlace => one click to create a service
          // <TextField
          //   className={classes.searchButton}
          //   id="searchValue"
          //   placeholder="Filtrar por albaran"
          //   value={searchValue}
          //   InputProps={{
          //     startAdornment: (
          //     <InputAdornment position="end">
          //       <IconButton
          //         aria-label="Toggle password visibility"
          //         onClick={this.onChangeSearchValue}
          //         // onMouseDown={this.handleMouseDownPassword}
          //       >
          //         <Icons.Search />
          //       </IconButton>
          //     </InputAdornment>
          //     ),
          //   }}
          // /> */}

          {/* <MenuItem className={classes.menuItem} onClick={this.goToNewService}>
            <ListItemIcon className={classes.icon}>
              <Icons.NoteAdd />
            </ListItemIcon>         
            <ListItemText classes={{ primary: classes.primary }} inset primary="Solicitar nuevo" />
          </MenuItem> */}
        </Tabs>
        <Menu
          id="long-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
          PaperProps={{
            style: {
              maxHeight: ITEM_HEIGHT * 4.5,
              width: 200,
            },
          }}>
          <MenuItem className={classes.menuItem} onClick={this.goToNewService}>
            <ListItemIcon className={classes.icon}>
              <Icons.NoteAdd />
            </ListItemIcon>
            <ListItemText classes={{ primary: classes.primary }} inset primary="Solicitar nuevo" />
          </MenuItem>
        </Menu>
        {
          servicesList.map((service, key) => {
            // TODO: Cada stado ha de tener su propio expanded item
            return (
              <ListItemService
                itemKey={key}
                expandedKey={expandedKey}
                item={service}
                carriers={carriers}
                fncServiceUpdate={this.onUpdatService}
                fcnOnExpand={this.onItemExpand}
              />
            )
          })
        }
      </div>
    )
  }
}

ServiceListPage.propTypes = {
  classes: PropTypes.object.isRequired,
  services: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.string,
  fcnLoadServices: PropTypes.func.isRequired,
  fcnAssingCarrierToServe: PropTypes.func.isRequired,
  fcnChangeStatusFiltered: PropTypes.func.isRequired,
  fcnChangeSearchValue: PropTypes.func.isRequired,
}

/* STATE SECTION */
function mapStateToProps(state) {
  const page = getStateServicesListPageFinal(state)
  return { page }
}

/* DISPATCH SECTION */
var mapDispatchToProps = (dispatch) => {
  return {
    fcnLoadServices: (userKey, isAdmin) => {
      dispatch(Actions.loadServices(userKey, isAdmin))
    },
    fcnAssingCarrierToServe: (serviceKey, updatedFields) => {
      dispatch(Actions.assingCarrierToServe(serviceKey, updatedFields))
    },
    fcnChangeStatusFiltered: (status) => {
      dispatch(Actions.changeStatusToFilter(status))
    },
    fcnChangeSearchValue: (searchValue) => {
      dispatch(Actions.changeSearchValue(searchValue))
    },
    fcnChangeExpanendItem: (expandedKey) => {
      dispatch(Actions.changeExpandedKey(expandedKey))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ServiceListPage))