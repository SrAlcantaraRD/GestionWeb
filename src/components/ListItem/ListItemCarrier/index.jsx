import React from 'react'
import PropTypes from 'prop-types'
import { TableCell, TableRow, IconButton, Menu, MenuItem, ListItemIcon, ListItemText } from '@material-ui/core'
import * as Icons from '@material-ui/icons'

class ListItemCarrier extends React.Component {
  constructor(props, context) {
    super(props, context)

    this.handleClick = this.handleClick.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleChangeBlock = this.handleChangeBlock.bind(this)
    this.state = {
      anchorEl: null,
    }
  }
  handleChangeBlock = () => {
    const { key, active, ...carrier } = this.props.carrier

    carrier.active = !active

    this.props.fcnUpdateCarrier(key, carrier)
  }
  handleClose = () => {
    this.setState({ anchorEl: null })
  }
  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget })
  }
  render() {
    const ITEM_HEIGHT = 48
    const { carrier, key } = this.props
    const { anchorEl } = this.state
    const IconToRender = carrier.active ? Icons.LockOpen : Icons.Lock
    return (
      <TableRow key={key} hover >
        <TableCell component="th" scope="row">
          {`${carrier.firtsName} ${carrier.lastName}`}
        </TableCell>
        <TableCell numeric>{carrier.phone}</TableCell>
        <TableCell component="th" scope="row">
          {carrier.nif}
        </TableCell>
        <TableCell numeric>{carrier.active ? 'Si' : 'No'}</TableCell>
        <TableCell numeric>
          <IconButton
            aria-label="More"
            aria-owns={anchorEl ? 'long-menu' : null}
            aria-haspopup="true"
            onClick={this.handleClick}>
            <Icons.MoreVert />
          </IconButton>
        </TableCell>
        <Menu
          id="long-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
          PaperProps={{
            style: {
              maxHeight: ITEM_HEIGHT * 4.5,
              width: 200,
            },
          }}>
          <MenuItem onClick={this.handleChangeBlock}>
            <ListItemIcon >
              <IconToRender color='primary' />
            </ListItemIcon>
            <ListItemText inset primary={carrier.active ? 'Bloquear' : 'Desbloquear'} />
          </MenuItem>
        </Menu>
      </TableRow>
    )
  }
}

ListItemCarrier.propTypes = {
  carrier: PropTypes.object.isRequired,
  key: PropTypes.string.isRequired,
  fcnUpdateCarrier: PropTypes.func.isRequired
}

export default ListItemCarrier
