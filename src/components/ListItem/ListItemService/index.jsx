import React from 'react'
import PropTypes from 'prop-types'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import SwipeableViews from 'react-swipeable-views'
import {
  ListServiceEvents,
  ListServicePackages,
  CarriersAsignationForm,
  CollectionForm,
  DeliveryForm,
} from 'Components'
import {
  ExpansionPanelSummary,
  ExpansionPanel,
  Typography,
  withStyles,
  Divider,
  Tabs,
  Grid,
  Tab,
  Stepper,
  Step,
  StepLabel,
} from '@material-ui/core'

const styles = theme => ({
  root: {
    width: '90%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  column: {
    flexBasis: '33.33%',
  },
  rootList: {
    width: '100%',
    maxHeight: 250,
    overflow: 'auto',
    backgroundColor: theme.palette.background.paper,
  },
  listSection: {
    backgroundColor: 'inherit',
  },
  ul: {
    backgroundColor: 'inherit',
    padding: 0,
  },
  backButton: {
    marginRight: theme.spacing.unit,
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
})

function getSteps() {
  return ['Asignación', 'Recogida', 'Entrega']
}


class ListItemService extends React.Component {
  constructor(props, context) {
    super(props, context)

    this.getStatus = this.getStatus.bind(this)
    this.handleCarrierChange = this.handleCarrierChange.bind(this)
    this.getStepContent = this.getStepContent.bind(this)
    this.state = {
      tabIndex: 0,
      isExpanded: false,
      activeStep: 0,
    }
  }

  getStatus = (intStatus) => {
    switch (intStatus) {
      case 0:
        return 'Pendiente'
      case 1:
        return 'Asignado'
      case 2:
        return 'En ruta'
      case 3:
        return 'Entregado'
      default:
        break
    }
  }

  handleCarrierChange = carrier => () => {
    this.setState({ selectedCarrier: carrier })
  }

  handleChange = (event, tabIndex) => {
    this.setState({ tabIndex })
  }

  onAssingCarrier = (carrierKey) => {
    const { history, key } = this.props.item

    const entitiesKeys = {
      serviceKey: key,
      updatedFields: {
        carrierKey: carrierKey,
        history: history,
        status: 1,
      }
    }

    this.props.fncServiceUpdate(entitiesKeys, 'assingCarrier')
  }

  onServiceCollected = (collectionPlaceID) => {
    const { history, key } = this.props.item

    const entitiesKeys = {
      serviceKey: key,
      updatedFields: {
        collectionPlaceID: collectionPlaceID,
        history: history,
        status: 2,
      }
    }

    this.props.fncServiceUpdate(entitiesKeys, 'packagesCollected')
  }

  onServiceDelivered = () => {
    const { history, key } = this.props.item

    const entitiesKeys = {
      serviceKey: key,
      updatedFields: {
        history: history,
        status: 3,
      }
    }

    this.props.fncServiceUpdate(entitiesKeys, 'serviceDelivered')
  }

  handleChangeExpandedKey = key => (event, expanded) => {
    this.props.fcnOnExpand(key)
    this.setState({
      isExpanded: expanded
    })
  }

  getStepContent = () => {
    const { item, carriers } = this.props
    switch (item.status) {
      case 0:
        return (
          <CarriersAsignationForm
            selectedKey={item.carrierKey}
            carrierList={carriers}
            onAssingCarrier={this.onAssingCarrier}
            canBeAssigned={!item.hasOwnProperty('carrierKey')} />
        )
      case 1:
        return (
          <CollectionForm
            assignedCarrier={carriers.find(carrier => carrier.key === item.carrierKey)}
            onServiceCollected={this.onServiceCollected}
          />
        )
      case 2:
        return (
          <DeliveryForm
            address={item.address}
            city={item.city}
            observations={item.observations}
            assignedCarrier={carriers.find(carrier => carrier.key === item.carrierKey)}
            onServiceDelivered={this.onServiceDelivered}
          />
        )
      default:
        return 'Uknown stepIndex'
    }
  }

  render() {
    const { item, classes, itemKey, expandedKey } = this.props
    const { isExpanded, tabIndex } = this.state
    const steps = getSteps()

    return (
      <ExpansionPanel
        expanded={isExpanded && itemKey === expandedKey}
        onChange={this.handleChangeExpandedKey(itemKey)}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <div className={classes.column}>
            <Typography className={classes.heading}>
              {item.deliveryNote}
            </Typography>
          </div>
          <div className={classes.column}>
            <Typography className={classes.secondaryHeading}>
              {`${item.clientName} (Tel: ${item.phone})`}
            </Typography>
          </div>
          <div className={classes.column}>
            <Typography className={classes.secondaryHeading}>
              {this.getStatus(item.status)}
            </Typography>
          </div>
        </ExpansionPanelSummary>
        <Divider />
        <Tabs
          value={tabIndex}
          onChange={this.handleChange}
          fullWidth
          indicatorColor='primary'
          textColor='primary'>
          <Tab label='Detalles' />
          <Tab label='Acciones' />
          {/* <Tab label='Asignación' disabled /> */}
        </Tabs>

        {
          isExpanded && <SwipeableViews
            axis={'x'}
            index={tabIndex}>
            <Grid container >
              <Grid item xs={4}>
                <ListServiceEvents historyList={item.history} />
              </Grid>
              <Grid item xs={8}>
                <ListServicePackages packageList={item.packages} />
              </Grid>
            </Grid>
            <div>
              {/* <Stepper activeStep={item.status} orientation="vertical"> */}
              <Stepper activeStep={item.status}>
                {steps.map(label => {
                  return (
                    <Step key={label}>
                      <StepLabel>{label}</StepLabel>
                    </Step>
                  )
                })}
              </Stepper>
              {this.getStepContent()}
            </div>
          </SwipeableViews>
        }
      </ExpansionPanel>
    )
  }
}

ListItemService.propTypes = {
  itemKey: PropTypes.number.isRequired,
  item: PropTypes.object.isRequired,
  carriers: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
  fncServiceUpdate: PropTypes.func.isRequired,
  fcnOnExpand: PropTypes.func.isRequired,
}

export default withStyles(styles)(ListItemService)
