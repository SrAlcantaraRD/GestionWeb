import Constants from './constants'
import { firebase } from 'Firebase'

const firestore = firebase.firestore
const collectionRef = firestore.collection("carriers")

const Actions = {
  updateCarrier
}

function updateCarrier(key, carrier) {
  var carrierRef = collectionRef.doc(key)

  return dispatch => {
    carrierRef.update({
      ...carrier
    }).then(function () {
      dispatch(success())
    })
      .catch(function (error) {
        dispatch(failure(error))
      });
  }


  function success() {
    return { type: Constants.CARRIER_LIST_PAGE_REQUEST_SUCCESS }
  }
  function failure(error) {
    return { type: Constants.CARRIERS_LIST_PAGE_REQUEST_FAILURE, error }
  }
}

export default Actions
