import React from 'react';
import PropTypes from 'prop-types';
import {Menu} from '@material-ui/icons';
import {
    withStyles,
    AppBar,
    Toolbar,
    IconButton,
    Hidden
} from '@material-ui/core';
import cx from 'classnames';

import headerStyle from 'styleJSS/headerStyle';

import HeaderLinks from './HeaderLinks';

function Header({
    ...props
}) {
    // function makeBrand() {
    //     var name;
    //     props
    //         .routes
    //         .map((prop, key) => {
    //             if (prop.path === props.location.pathname) {
    //                 name = prop.navbarName;
    //             }
    //             return null;
    //         });
    //     return name;
    // }
    const {classes, color} = props;
    const appBarClasses = cx({[' ' + classes[color]]: color});
    return (
        <AppBar className={classes.appBar + appBarClasses}>
            <Toolbar className={classes.container}>
                <Hidden smDown implementation='css'>
                    <HeaderLinks/>
                </Hidden>
                <Hidden mdUp>
                    <IconButton
                        className={classes.appResponsive}
                        color='inherit'
                        aria-label='open drawer'
                        onClick={props.handleDrawerToggle}>
                        <Menu/>
                    </IconButton>
                </Hidden>
            </Toolbar>
        </AppBar>
    );
}

Header.propTypes = {
    routes: PropTypes.array,
    classes: PropTypes.object.isRequired,
    location: PropTypes.object,
    handleDrawerToggle: PropTypes.func,
    color: PropTypes.oneOf(['primary', 'info', 'success', 'warning', 'danger'])
};

export default withStyles(headerStyle)(Header);
