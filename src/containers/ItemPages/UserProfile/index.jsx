import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Actions from './actions'
import { getStateUserProfilePagepPageFinal } from './selectors'
import { Alerts, ProfileForm } from 'Components'

class UserProfilePage extends Component {
  constructor(props) {
    super(props)
    this.updateByPropertyName = this.updateByPropertyName.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.confirmChanges = this.confirmChanges.bind(this)
  }

  confirmChanges = (event) => {
    this.props.fcnIsConfirmIsVisible(true)
    event.preventDefault()
  }

  onSubmit = () => {
    const userData = this.props.page
    this.props.fcnIsRequestPending()
    this.props.fcnOnSubmit(userData)
  }

  updateByPropertyName = (event) => {
    const { id, value } = event.target
    this.props.fcnUpdateByPropertyName(id, value)
  }

  render() {
    const { isUpdated, isPending, isConfirming } = this.props.page
    return (
      <div>
        <Alerts.Confirm
          boolOpen={isConfirming}
          strTitle={'Confirmación de Registro'}
          strDescription={`
            Caminante, son tus huellas el camino y nada más.
            Caminante, no hay camino, se hace camino al andar.
            Caminante no hay camino sino estelas en la mar.`
          }
          strNonAgree={'Cancelar'}
          strAgree={'Aceptar'}
          fcnOnAccept={this.onSubmit}
          fcnOnCancel={this.props.fcnIsConfirmIsVisible} />
        <Alerts.Loading
          boolOpen={isPending}
          strTitle={'Guardando datos'} />
        <Alerts.Success
          boolOpen={isUpdated}
          strTitle={'Datos actualizados'}
          strAgree={'Continuar'}
          fcnOnAccept={() => {this.props.fcnUpdateByPropertyName('isUpdated', false)}} />
        <ProfileForm
          {...this.props.page}
          fcnUpdateByPropertyName={this.updateByPropertyName}
          fcnConfirmChanges={this.confirmChanges} />
      </div>
    )
  }
}

UserProfilePage.propTypes = {
  fcnUpdateByPropertyName: PropTypes.func.isRequired,
  fcnOnSubmit: PropTypes.func.isRequired,
  fcnIsConfirmIsVisible: PropTypes.func.isRequired,
  fcnIsRequestPending: PropTypes.func.isRequired,
}

/* STATE SECTION */
function mapStateToProps(state) {
  const page = getStateUserProfilePagepPageFinal(state) 
  return { page }
}

/* DISPATCH SECTION */
var mapDispatchToProps = (dispatch) => {
  return {
    fcnUpdateByPropertyName: (property, value) => {
      dispatch(Actions.onChaneFormInput(property, value))
    },
    fcnOnSubmit: (userData) => {
      dispatch(Actions.onSubmit(userData))
    },
    fcnIsConfirmIsVisible: (isVisible = false) => {
      dispatch(Actions.isConfirmingVisible(isVisible))
    },
    fcnIsRequestPending: (isVisible = true) => {
      dispatch(Actions.isRequestPending(isVisible))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfilePage)
