import Constants from './constants'
import { firebase } from 'Firebase'

const firestore = firebase.firestore
// const userKey = 'oPkY78s0UYYjRfWlSsLS7cuumVh2'
// const collectionRef = firestore.collection("services").where("userKey", '==', userKey)
let collectionRef = firestore.collection("services")

const Actions = {
  loadServices,
  assingCarrierToServe,
  changeStatusToFilter,
  changeSearchValue,
  changeExpandedKey
}

function changeExpandedKey(expandedKey){
  return dispatch => {
    dispatch({ type: Constants.SERVICES_LIST_PAGE_CHANGE_EXPANDED_KEY, expandedKey })
  }
}

function changeSearchValue(searchValue){
  return dispatch => {
    dispatch({ type: Constants.SERVICES_LIST_PAGE_CHANGE_SEARCH_VALUE, searchValue })
  }
}

function changeStatusToFilter(statusToFilter) {
  return dispatch => {
    dispatch({ type: Constants.SERVICES_LIST_PAGE_CHANGE_STATUS_TO_FILTER, statusToFilter })
  }
}

function assingCarrierToServe(serviceKey, updatedFields) {
  var serviceRef = collectionRef.doc(serviceKey)
  return dispatch => {
    serviceRef.set({
      ...updatedFields
    }, { merge: true }).then(function () {
      dispatch(success())
    })
      .catch(function (error) {
        dispatch(failure(error))
      });
  }

  function success() {
    return { type: Constants.SERVICES_LIST_PAGE_SAVE_SUCCESS }
  }
  function failure(error) {
    return { type: Constants.SERVICES_LIST_PAGE_REQUEST_FAILURE, error }
  }
}

function loadServices(userKey, isAdmin) {
  let collection = collectionRef

  if (!isAdmin) {
    collection = collectionRef.where("userKey", '==', userKey)
  }

  return dispatch => {
    collection.onSnapshot(function (snapshot) {
      var servicesLists = [[], [], [], []]

      snapshot.forEach(function (doc) {
        const service = {
          key: doc.id,
          ...doc.data(),
        }
        servicesLists[service.status].push(service)
      })
      const tabConfiguration = [
        { status: 0, quantity: servicesLists[0].length, description: 'Sin asignación' },
        { status: 1, quantity: servicesLists[1].length, description: 'En espera' },
        { status: 2, quantity: servicesLists[2].length, description: 'En ruta' },
        { status: 3, quantity: servicesLists[3].length, description: 'Entregados' },
      ]

      dispatch(saveServiceCount(tabConfiguration))
      dispatch(success(servicesLists))
    }, function (error) {
      dispatch(failure(error))
    })
  }

  function saveServiceCount(tabConfiguration){
    return { type: Constants.SERVICES_LIST_PAGE_SET_TAB_CONFIGURATION, tabConfiguration }
  }
  function success(services) {
    return { type: Constants.SERVICES_LIST_PAGE_PACKAGES_LOADED, services }
  }
  function failure(error) {
    return { type: Constants.SERVICES_LIST_PAGE_REQUEST_FAILURE, error }
  }
}

export default Actions
