import React, { Component } from 'react'
import { withRouter, Link } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import { AppBar, Toolbar, Button, Typography } from '@material-ui/core'
import * as routes from 'Routes'

const styles = theme => ({
  button: {
    float: 'right',
    margin: theme.spacing.unit * 3
  }
})

class LandingPage extends Component {

  render() {
    const { classes } = this.props
    return (
      <div>
        <AppBar position="fixed">
          <Toolbar>
            <Typography variant="title" color="inherit" className={classes.flex}>
              GestiónWeb
            </Typography>
            <Button
              color='inherit'
              className={classes.button}
              to={routes.SIGN_IN}
              component={Link}>
              Acceder
            </Button>
            <Button
              color='inherit'
              className={classes.button}
              to={routes.SIGN_UP}
              component={Link}>
              Registrarse
            </Button>
          </Toolbar>
        </AppBar>
      </div>
    )
  }
}

export default withRouter(withStyles(styles)(LandingPage))
