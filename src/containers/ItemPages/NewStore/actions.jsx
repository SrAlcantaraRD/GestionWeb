import Constants from './constants';
import { auth, firebase } from 'Firebase'

const firestore = firebase.firestore
/** 
  TODO: Cambiar flujo de creación de usuarios
    1. Todos los usuarios se guardarán en la colección "users"
      - clave de autentificación
        - datos personales
        - type
    2. Todo usuario tendrá la propiedad "type". Que puede ser:
      - stores
      - storesClients => en el futuro
      - carriers
      - employees => de momento, sólo el "admin" 
        - isAdmin (?)
      * Este valor marcará las paginas/acciones accesible por el usuario
*/
const collectionRef = firestore.collection("stores")

const Actions = {
  onChaneFormInput,
  addStore,
  isConfirmingVisible,
  restartForm,
}

function restartForm() {
  return dispatch => {
    dispatch({ type: Constants.NEW_STORE_RESTART_FORM });
  }
}

function isConfirmingVisible(isConfirming = false) {
  return dispatch => {
    dispatch({ type: Constants.NEW_STORE_CHANGE_CONFIRMING, isConfirming });
  }
}

function onChaneFormInput(property, value) {
  const item = {
    name: property,
    value: value
  }
  return dispatch => {
    dispatch({ type: Constants.NEW_STORE_CHANGE_INPUT, item });
  }
}

function addStore(store) {
  return dispatch => {

    dispatch(savingData(true))

    collectionRef.add({ ...store })
      .then(function () {
        dispatch(success())
      })
      .catch(function (error) {
        dispatch(failure(error))
      });
  }

  function savingData(isPending) {
    return { type: Constants.NEW_STORE_REQUEST_PENDING, isPending }
  }
  function success() {
    return { type: Constants.NEW_STORE_REQUEST_SUCCESS }
  }
  function failure(error) {
    return { type: Constants.NEW_STORE_REQUEST_FAILURE, error }
  }
}

export default Actions;
