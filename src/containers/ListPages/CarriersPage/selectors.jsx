import { createSelector } from 'reselect';

const selectCarriersListPageDomain = () => state => state.listPages.carriers

const getStateCarriersListPageFinal = createSelector(
  selectCarriersListPageDomain(),
  (carriersListPageList) => carriersListPageList
);

export {
  selectCarriersListPageDomain,
  getStateCarriersListPageFinal,
};