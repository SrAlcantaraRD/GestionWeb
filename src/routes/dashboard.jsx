// MainPage
import DashboardPage from 'Containers/Dashboard'

// ListPages
import ListPageServices from 'Containers/ListPages/ServicesPage'
import ListPageStores from 'Containers/ListPages/StoresPage'
import ListPageCarriers from 'Containers/ListPages/CarriersPage'

// ItemPages
import ItemPageCarrier from 'Containers/ItemPages/Carrier'
import ItemPageUserProfile from 'Containers/ItemPages/UserProfile'
import ItemPageNewService from 'Containers/ItemPages/NewService'
import ItemPageNewStore from 'Containers/ItemPages/NewStore'

// Others
import { PageBuilding } from 'Components'

import {
  Dashboard,
  Assignment,
  Home,
  Store,
  SupervisorAccount,
  LocalShipping
} from '@material-ui/icons'

const dashboardRoutes = [
  // MainPage  
  {
    path: '/Dashboard',
    sidebarName: 'Inicio',
    navbarName: 'Inicio',
    icon: Home,
    component: DashboardPage
  },

  // ListPages
  {
    path: '/Services',
    sidebarName: 'Mis Servicios',
    navbarName: 'Mis Servicios',
    icon: Dashboard,
    component: ListPageServices
  }, {
    path: '/Carriers',
    sidebarName: 'Mis Clientes',
    navbarName: 'Mis Clientes',
    icon: SupervisorAccount,
    component: ListPageCarriers
  }, {
    path: '/Stores',
    sidebarName: 'Tiendas',
    navbarName: 'Tiendas',
    icon: Store,
    component: ListPageStores
  },

  // ItemPages
  {
    path: '/NewCarrier',
    sidebarName: 'Mis Clientes',
    navbarName: 'Mis Clientes',
    icon: SupervisorAccount,
    component: ItemPageCarrier
  }, {
    path: '/NewService',
    sidebarName: 'Mis Clientes',
    navbarName: 'Mis Clientes',
    icon: SupervisorAccount,
    component: ItemPageNewService
  }, {
    path: '/NewStore',
    sidebarName: 'Crear tienda',
    navbarName: 'Crear tienda',
    icon: SupervisorAccount,
    component: ItemPageNewStore
  }, {
    path: '/UserProfile',
    sidebarName: 'Mis Clientes',
    navbarName: 'Mis Clientes',
    icon: SupervisorAccount,
    component: ItemPageUserProfile
  }, {
    path: '/Control',
    sidebarName: 'Administración',
    navbarName: 'Administración',
    icon: Assignment,
    component: PageBuilding
  }
]

export default dashboardRoutes