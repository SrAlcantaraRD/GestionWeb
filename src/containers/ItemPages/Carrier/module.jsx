import Constants from './constants';

export const initialState = {
  firtsName: '',
  lastName: '',
  phone: '',
  nif: '',
  active: true,
  error: {},
  isPending: false,
  isConfirming: false,
  isUpdated: false,
};

export default function CarrierPageReducer(state = initialState, action) {
  switch (action.type) {
    case Constants.CARRIER_PAGE_CHANGE_INPUT:
      const newData = []
      newData[action.item.name] = action.item.value
      return Object.assign({}, state, newData)
    case Constants.CARRIER_PAGE_CHANGE_CONFIRMING:
      return Object.assign({}, state, { isConfirming: action.isConfirming })
    case Constants.CARRIER_PAGE_REQUEST_PENDING:
      return Object.assign({}, state, { isPending: true, error: {}, isConfirming: false })
    case Constants.CARRIER_PAGE_REQUEST_FAILURE:
      return Object.assign({}, state, { error: action.error, isPending: false })
    case Constants.CARRIER_PAGE_REQUEST_SUCCESS:
      return Object.assign({}, state, { isPending: false, isUpdated: true })
    default:
      return state;
  }
}
