import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Actions from './actions'
import Selectors from './selectors'
import { Alerts, NewStoreForm } from 'Components'
import { history } from 'Helpers'
import * as routes from 'Routes'
import { Snackbar } from '@material-ui/core'

class NewStorePage extends Component {
  constructor(props) {
    super(props)
    this.updateByPropertyName = this.updateByPropertyName.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.goToHome = this.goToHome.bind(this)
  }

  goToHome = () => {
    this.props.fcnRestartForm()
    history.push(routes.STORE_LIST_PAGE)
  }

  confirmRegistration = (event) => {
     this.props.fcnIsConfirmIsVisible(true)
    event.preventDefault()
  }

  onSubmit = () => {
    console.log('klk')
    const newStore = this.props.page.storeData
    this.props.fcnSaveStore(newStore)
  }

  updateByPropertyName = (event) => {
    const { id, value } = event.target
    this.props.fcnUpdateByPropertyName(id, value)
  }

  render() {
    const {
      isCreated,
      isPending,
      isConfirming,
      isSubmited,
      storeData,
      error,
    } = this.props.page

    return (
      <div style={{ paddingTop: '100px' }}>
        <Alerts.Confirm
          boolOpen={isConfirming}
          strTitle={'Confirmación de Registro'}
          strDescription={`
            Caminante, son tus huellas el camino y nada más.
            Caminante, no hay camino, se hace camino al andar.
            Caminante no hay camino sino estelas en la mar.`
          }
          strNonAgree={'Cancelar'}
          strAgree={'Aceptar'}
          fcnOnAccept={this.onSubmit}
          fcnOnCancel={this.props.fcnIsConfirmIsVisible} />
        <Alerts.Loading
          boolOpen={isPending}
          strTitle={'Guardando datos'} />
        <Snackbar
          anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
          open={isCreated}
          autoHideDuration={6000}
          onClose={this.goToHome}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">Tienda creada.</span>}
        />
        <NewStoreForm
          storeData={storeData}
          isSubmited={isSubmited}
          isCreated={isCreated}
          error={error}
          fcnUpdateByPropertyName={this.updateByPropertyName}
          confirmRegistration={this.confirmRegistration} />
      </div>
    )
  }
}

NewStorePage.propTypes = {
  fcnUpdateByPropertyName: PropTypes.func.isRequired,
  fcnSaveStore: PropTypes.func.isRequired,
  fcnIsConfirmIsVisible: PropTypes.func.isRequired,
  fcnRestartForm: PropTypes.func.isRequired,
}

/* STATE SECTION */
function mapStateToProps(state) {
  const page = Selectors.getStateNewStorePageFinal(state)
  return { page }
}

/* DISPATCH SECTION */
var mapDispatchToProps = (dispatch) => {
  return {
    fcnUpdateByPropertyName: (property, value) => {
      dispatch(Actions.onChaneFormInput(property, value))
    },
    fcnSaveStore: (newStore) => {
      dispatch(Actions.addStore(newStore))
    },
    fcnIsConfirmIsVisible: (isVisible = false) => {
      dispatch(Actions.isConfirmingVisible(isVisible))
    },
    fcnRestartForm: () => {
      dispatch(Actions.restartForm())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewStorePage)
