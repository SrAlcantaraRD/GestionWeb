import * as React from 'react'
import PropTypes from 'prop-types'
import { TextField, Button } from '@material-ui/core'
import { AddressSearchBar } from 'Components'
class ReceiverForm extends React.Component {
  constructor(props) {
    super(props)
    this.handleBack = this.handleBack.bind(this)
    this.handleNext = this.handleNext.bind(this)
  }

  handleNext = () => {
    this.props.fncNextStep()
  }

  handleBack = () => {
    this.props.fcnPreviouStep()
  }

  render() {
    const { deliveryNote, clientName, phone, nif, city, address, classes } = this.props
    const cannotAdvance = !deliveryNote || !clientName || !phone || !nif || !city || !address
    return (
      <div>
        <TextField
          id="deliveryNote"
          label="Albarán"
          required
          className={classes.textField}
          value={deliveryNote.toUpperCase()}
          error={!deliveryNote}
          onChange={this.props.onChangeInput}
          margin="normal" />
          {/* TODO: Usar AddressSearchBar para que las direcciones existan(Dar formato) */}
        {/* <AddressSearchBar/> */}
        <TextField
          id="clientName"
          label="Nombre y Apellidos"
          className={classes.textField}
          value={clientName}
          error={!clientName}
          onChange={this.props.onChangeInput}
          margin="normal" />
        <TextField
          id="nif"
          label="N.I.F"
          className={classes.textField}
          value={nif}
          error={!nif}
          onChange={this.props.onChangeInput}
          margin="normal" />
        <TextField
          id="phone"
          label="Telefóno"
          className={classes.textField}
          value={phone}
          error={!phone}
          // TODO: ADD https://material-ui.com/demos/text-fields/#formatted-inputs
          onChange={this.props.onChangeInput}
          margin="normal" />
        <TextField
          id="city"
          label="Ciudad"
          className={classes.textField}
          value={city}
          error={!city}
          onChange={this.props.onChangeInput}
          margin="normal" />
        <TextField
          id="address"
          label="Dirección"
          className={classes.textField}
          value={address}
          error={!address}
          onChange={this.props.onChangeInput}
          margin="normal" />
        <Button
          onClick={this.handleBack}
          className={classes.button}>
          Anterior
        </Button>
        <Button
          variant="contained"
          color="primary"
          disabled={cannotAdvance}
          onClick={this.handleNext}
          className={classes.button}
        >
          Siguiente
        </Button>
      </div>
    )
  }
}

ReceiverForm.propTypes = {
  name: PropTypes.string,
  phone: PropTypes.string,
  nif: PropTypes.string,
  city: PropTypes.string,
  address: PropTypes.string,
  classes: PropTypes.object,
  onChangeInput: PropTypes.func.isRequired,
  fncNextStep: PropTypes.func.isRequired,
  fcnPreviouStep: PropTypes.func.isRequired,
}


export default ReceiverForm