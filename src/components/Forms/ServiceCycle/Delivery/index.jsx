import * as React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import {
  TextField,
  Button,
  Paper,
  Grid,
  ListSubheader,
  Divider,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
} from '@material-ui/core'



const appStyle = theme => ({
  root: {
    width: '90%',
  },
  rootPaper: {
    margin: theme.spacing.unit,
    padding: theme.spacing.unit,
    maxHeight: 230,
    minHeight: 230,
    overflow: 'auto',
    backgroundColor: theme.palette.background.paper,
  },
  formControl: {
    margin: theme.spacing.unit * 2,
    width: '90%',
    margin: 'auto',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%',
  },
})

class DeliveryForm extends React.Component {
  constructor(props) {
    super(props)
    this.onClickHandle = this.onClickHandle.bind(this)
  }

  onClickHandle = () => {
    this.props.onServiceDelivered()
  }

  handleChange = event => {
    this.setState({ value: event.target.value })
  }

  render() {
    const { classes, assignedCarrier, address, city, observations } = this.props
    const { firtsName, lastName, phone } = assignedCarrier
    return (
      <Grid container>
        <Grid item xs={4}>
          <Paper className={classes.rootPaper}>
            <ListSubheader>Transportista</ListSubheader>
            <Divider />
            <FormControl component='fieldset' className={classes.formControl}>
              <TextField
                id='name'
                label='Nombre y Apellidos'
                className={classes.textField}
                value={`${firtsName} ${lastName}`}
                type='text'
                margin='normal'
                InputProps={{
                  readOnly: true,
                }}
              />
              <TextField
                id='phone'
                label='Telefono'
                className={classes.textField}
                value={`${phone}`}
                type='text'
                margin='normal'
                InputProps={{
                  readOnly: true,
                }}
              />
            </FormControl>
          </Paper>
        </Grid>
        <Grid item xs={8}>
          <Paper className={classes.rootPaper}>
            <ListSubheader>Datos del Cliente</ListSubheader>
            <Divider />
            <FormControl component='fieldset' required className={classes.formControl}>
              {/* 
                TODO: Añadir botón para abrir ubicación con google maps 
                {`https://www.google.com/maps/search/?api=1&query=${latitude},${longitude}`}
            */}
              <TextField
                id='address'
                label='Dirección'
                className={classes.textField}
                value={`${city.toUpperCase()}: ${address}`}
                type='text'
                fullWidth
                margin='normal'
                InputProps={{
                  readOnly: true,
                }}
              />
              {
                observations && <TextField
                  id='phone'
                  label='Observaciones'
                  className={classes.textField}
                  value={observations}
                  multiline
                  fullWidth
                  type='text'
                  margin='normal'
                  InputProps={{
                    readOnly: true,
                  }}
                />
              }
              {/* 
                TODO: Comprobar que la posición actual del usuario es cercana a la posición de cliente
                https://material-ui.com/demos/progress/#interactive-integration
              */}
              <Button
                variant='contained'
                type='submit'
                color='primary'
                // disabled={!value}
                onClick={this.onClickHandle}
                className={classes.button}>
                Confirmar entrega
            </Button>
            </FormControl>
          </Paper>
        </Grid>
      </Grid>
    )
  }
}

DeliveryForm.propTypes = {
  address: PropTypes.string.isRequired,
  city: PropTypes.string.isRequired,
  observations: PropTypes.string,
  classes: PropTypes.object.isRequired,
  onServiceDelivered: PropTypes.func.isRequired,
}

export default withStyles(appStyle)(DeliveryForm)