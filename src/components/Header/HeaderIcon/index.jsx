import * as React from 'react'
import PropTypes from 'prop-types'
import { Button, Tooltip } from '@material-ui/core'

class HeaderIcon extends React.Component {
  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this)
  }

  onClick(e) {
    this.props.onClick()
    e.preventDefault()
  }

  render() {
    const { componentIcon, toolTip } = this.props
    const Icon = componentIcon

    return (
      <Tooltip title={toolTip}>
        <Button
          color='inherit'
          onClick={this.onClick}>
          <Icon />
        </Button>
      </Tooltip>
    )
  }
}

HeaderIcon.propTypes = {
  onClick: PropTypes.func.isRequired,
  toolTip: PropTypes.string,
  componentIcon: PropTypes.func.isRequired,
}

export default HeaderIcon
