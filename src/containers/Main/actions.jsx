import Constants from './constants';
import { auth } from 'Firebase'

const Actions = {
  changeMobileOpen,
  logOut,
}

function changeMobileOpen() {
  return dispatch => {
    dispatch({ type: Constants.MAIN_PAGE_CHANGE_MOBIL_OPEN });
  }
}

function logOut() {

  auth.doSignOut()

  return dispatch => {
    dispatch({ type: Constants.APP_USER_LOGOUT });
  }
}

export default Actions;
