import { createSelector } from 'reselect'

const selectNewStorePageDomain = () => state => state.itemPages.newStore
const getStateNewStorePageFinal = createSelector(
  selectNewStorePageDomain(),
  (newStore) => newStore
)

const Selectors = {
  selectNewStorePageDomain,
  getStateNewStorePageFinal,
}

export default Selectors