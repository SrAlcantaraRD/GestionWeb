import Constants from './constants'
import { firebase } from 'Firebase'

const firestore = firebase.firestore
const collectionRef = firestore.collection("users")

const Actions = {
  onChaneFormInput,
  onSubmit,
  isRequestPending,
  isConfirmingVisible,
}

function isConfirmingVisible(isConfirming = false) {
  return dispatch => {
    dispatch({ type: Constants.USER_PROFILE_CHANGE_CONFIRMING, isConfirming })
  }
}

function isRequestPending(isPending = false) {
  return dispatch => {
    dispatch({ type: Constants.USER_PROFILE_REQUEST_PENDING, isPending })
  }
}

function onChaneFormInput(property, value) {
  const item = {
    name: property,
    value: value
  }

  return dispatch => {
    dispatch({ type: Constants.USER_PROFILE_CHANGE_INPUT, item })
  }
}

function onSubmit(userData) {
  const { uid } = firebase.auth.currentUser
  var userRef = collectionRef.doc(uid)

  return dispatch => {
    userRef.update({
      company: userData.company,
      nif: userData.nif,
      phone: userData.phone,
      contactPerson: userData.contactPerson,
      city: userData.city,
      address: userData.address,
      postalCode: userData.postalCode
    }).then(function () {
      dispatch(success())
    })
      .catch(function (error) {
        dispatch(failure(error))
      });
  }


  function success() {
    return { type: Constants.USER_PROFILE_REQUEST_SUCCESS }
  }
  function failure(error) {
    return { type: Constants.USER_PROFILE_REQUEST_FAILURE, error }
  }
}

export default Actions
